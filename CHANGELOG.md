# [1.4.0](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.3.1...1.4.0) (2024-11-25)


### Bug Fixes

* corrige ícone de edição de link ([e87373a](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/e87373aa255a9cd2170148aa1858a52e3fa90261))
* corrige package-lock ([69b4362](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/69b4362941ede37a6946077242e9182e94a35271))


### Features

* adiciona botão para inserção e edição de link externo ([b4316de](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/b4316ded7ee06f27f8599777b6363170c9f3016f))

## [1.3.1](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.3.0...1.3.1) (2024-11-05)


### Bug Fixes

* adiciona prop para não atualizar editor ao alterar variáveis dinâmicas ([7d3156e](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7d3156ec1c80cca4fe428994017c338309410833))

# [1.3.0](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.6...1.3.0) (2024-11-05)


### Bug Fixes

* atualiza readme ([3ad9809](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/3ad9809a5c260129ea95498d5f35f862add8ee79))


### Features

* permite visualizar links ([67652d3](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/67652d332a026671baa8c36c621de81d4cbaa74f))

## [1.2.6](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.5...1.2.6) (2023-11-30)


### Bug Fixes

* corrige em partes o bug nas variaveis do editor ([e4d663b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/e4d663b092ce6f930485c77527730dd613da132d))
* corrige remoção de variavel dinamica que não existe ([50f5fac](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/50f5fac490b175faec1dbd208a12de58c197e78a))


### Code Refactoring

* apaga comentários ([93c81d5](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/93c81d52c5ac3c12b19e96e005acef3e638de4a8))

## [1.2.5](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.4...1.2.5) (2023-10-04)


### Bug Fixes

* ajuste ao inserir equacao ([34130bc](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/34130bc5ba335b914a1a9f62c26ad2240e46aed1))
* ajuste na edicao de equacao ([702a7a2](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/702a7a2a61573097a8d01aa634e0671f2f0cd51c))


### Code Refactoring

* corrige erro de lint ([7005b4f](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7005b4f52f3b7b8b62fb60fef53f59691f0c3e97))

## [1.2.4](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.3...1.2.4) (2023-09-15)


### Bug Fixes

* corrige alinhamento padrão no deserializer ([d2c97b6](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/d2c97b6e3679ed76544dc1e95d6e96476770319f))
* corrige lentidão no editor ([9e7f9ad](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/9e7f9ad249ec4344776d742a63a8e1f491e9b5fe))


### Code Refactoring

* melhora onClickToolbar ([1fd045a](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/1fd045a73df5d1b459fcbcc59d27cbb21c01adb5))
* remove duplicação de código ([7853c1d](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7853c1d5ee4661f225f55aa4cdd284417f54f467))

## [1.2.3](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.2...1.2.3) (2023-09-12)


### Bug Fixes

* adiciona compatibilidade com string sem tags html ([65250a0](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/65250a0ac83899ef9273aac5d6a859720a2b5c22))
* adiciona serializer no onKeyUp e ao clicar em botão do toolbar ([f14f68b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/f14f68b408c8ae66839aebbdc794d41a3ba0a4d6))
* corrige erros no onKeyUp e onClickToolbar ([6ba3e14](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/6ba3e14a16437353b47229c3bb3eb2727ef67306))


### Code Refactoring

* permite habilitar e desabilitar módulos do editor ([3c8de5c](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/3c8de5cbbf7ef58de7a95b9806dd01853864c408))

## [1.2.2](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.1...1.2.2) (2023-08-20)


### Bug Fixes

* borda arrendondada no componente do youtube ([90f85c6](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/90f85c64d4753df30ef5649cafe58ad51ad05dc2))
* corrige caso em que o onExit do textField não recebia videoId ([828460d](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/828460d5af5f7f0d8213cc464455507e865114e0))
* corrige erros de console em relação ao componente YoutubeVideo ([c31b335](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/c31b33534152e70e45c5afd9f011fb0ce7ccec74))

## [1.2.1](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.2.0...1.2.1) (2023-08-07)


### Bug Fixes

* corrige bug ao apagar node dentro de um container ou tabela ([e4ab1a7](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/e4ab1a738456b070bc9ff2477071a051586ea034))

# [1.2.0](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.1.0...1.2.0) (2023-08-07)


### Features

* permite excluir paragrafo vazio antes ou após um bloco ([9c23cc6](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/9c23cc6a64245c3e5fa7728f9e4f35428ed798eb))

# [1.1.0](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.8...1.1.0) (2023-08-07)


### Bug Fixes

* corrige espaço em branco abaixo do componente youtube e correções nos atributos ([7c6df9c](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7c6df9cace04321cf31bbfbb4e58f96c9189f7f3))
* remove loading do deserialize ([ad79b99](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/ad79b99e0b5524e99c3c9c20fdf4bd663e1f8630))


### Features

* adicionado funcionalidade de videos do youtube ([e771d1b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/e771d1b1dc2cc69ccb7d909a0dc034d51d2b7925))

## [1.0.8](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.7...1.0.8) (2023-06-16)


### Bug Fixes

* troca tag br por parágrafo vazio ([e1420ac](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/e1420ac14e91c316dca7f5b215496d648446c266))


### Code Refactoring

* corrige função de gerar parágrafo em branco ([7a004ae](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7a004aef8ea44c9638b2f49f88e1414cfb64cea5))

## [1.0.7](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.6...1.0.7) (2023-05-23)


### Bug Fixes

* adiciona tamanho padrão em parágrafo na tabela ([7de7b31](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/7de7b31dbccfd1f29a847ab3a63fa6cd4cafb7d6))
* corrige bug ao adicionar parágrafo abaixo ([6b4edb3](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/6b4edb33d0e0f56562b91ba086ae2bbecd3aff3c))
* corrige erros no editor ([50ff41b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/50ff41bb520fed53652134e955cae48621cbfced))
* corrige foco ao selecionar editor ([2691d37](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/2691d37522a100dab89c2fbdca2c4e07e0927a21))
* corrige foco no video e no áudio ([26e437b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/26e437bcb452db5ccf70c652e9dc0ed08eaca455))
* corrige função no onKeyDown ([ae04138](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/ae04138622768d3ef93bf5db54db18ceed47d83f))
* corrige selection ao adicionar linha/coluna na tabela ([28ba73a](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/28ba73af965fd8c36b83b5d8f1286dcab8e72f4b))
* focar próximo elemento ao apertar "delete" se não houver parágrafo ([14d472b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/14d472be8a59737ee8e4c02056e43578eec590e3))

## [1.0.6](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.5...1.0.6) (2023-05-12)


### Bug Fixes

* corrige adição de arquivo quando cursor está em um texto ([8820c24](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/8820c24377b0b2b4299c7ecb2211bf53a89fd226))
* corrige adição de elementos pela primeira vez em editor ([768945b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/768945bf483b6dc3f8491721653aa8cfc1d7fab1))
* corrige bug ao tornar expressão matemática em linha ([2f06a28](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/2f06a28900c64f4361e9d51321d0f7bdbe38b17f))

## [1.0.5](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.4...1.0.5) (2023-05-08)


### Bug Fixes

* corrige erro de loop infinito no componentDidUpdate ([9e320aa](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/9e320aa1e0803b8e14a34f15a467c4b1e84c041b))

## [1.0.4](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.3...1.0.4) (2023-05-07)


### Bug Fixes

* corrige comportamento de apagar nas células da tabela ([492d03b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/492d03b5543e6028d1ae8c8a712187931cd34a53))
* corrige erro ao duplicar texto ([3020c63](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/3020c63463daaa6bb59c7e7a3bf3dd936689f1c5))
* corrige erros no onKeyDown ([53f7f3a](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/53f7f3ad799d6c13be34835b5408558cf10f3a49))
* corrige largura inicial ao adicionar imagem/vídeo no editor e na tabela ([cf1bf3b](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/cf1bf3bfe6c44aee97dcfcaaec14cf0490c2b9ea))

## [1.0.3](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.2...1.0.3) (2023-05-03)


### Bug Fixes

* remove state no onFocus e onBlur ([cd2bf47](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/cd2bf4752e084a68321771a1311ec8ab7c3acc49))

## [1.0.2](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.1...1.0.2) (2023-04-30)


### Bug Fixes

* corrige erro que ocorre ao buscar elemento no editor sem foco ([68dfe98](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/68dfe98a97ce5a74e5c947ad28b03b406fc3ac0d))

## [1.0.1](https://gitlab.com/tapioca-ufrn/multiprova/editor/compare/1.0.0...1.0.1) (2023-04-28)


### Bug Fixes

* muda versão do node para 16 no gitlab-ci ([c3393b6](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/c3393b6d1757f03ac11b402322c7a114bf9331c1))

# 1.0.0 (2023-04-28)


### Bug Fixes

* corrige alinhamento horizontal em tabela e container ([0b6f7d2](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/0b6f7d26995f90158fbe85bb037515898e71998f))
* corrige instruções no readme ([b09f2b7](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/b09f2b7c5a0c54d4cc6705dade608a9fb98bce65))


### Code Refactoring

* corrige erro de lint ([8e9bad9](https://gitlab.com/tapioca-ufrn/multiprova/editor/commit/8e9bad9e85194f72cf72c1d98011f0afb6f671c3))
