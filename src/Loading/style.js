export const style = () => ({
  root: {
    position: 'relative',
    '&.active > :not(:first-child)': { opacity: 0.5 },
  },
  progress: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    margin: 'auto',
  },
})
