import React from 'react'
import classnames from 'classnames'

import CircularProgress from '@material-ui/core/CircularProgress'

import { propTypes } from './propTypes'

export const LoadingComponent = ({ classes, active, children }) => (
  <div className={classnames(classes.root, { active })}>
    {active && <CircularProgress className={classes.progress} />}
    {children}
  </div>
)

LoadingComponent.propTypes = propTypes
