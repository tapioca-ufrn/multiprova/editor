export const style = () => ({
  root: (props) => ({
    width: '100%',
    '& .MuiTypography-colorError': {
      color: props.theme === 'highContrast' ? 'white' : '#f44336',
    },
  }),
  label: {
    fontFamily: 'Roboto',
    fontSize: '16px',
    lineHeight: '18.75px',
    fontWeight: '400',
    marginBottom: 17,
  },
  container: {
    position: 'relative',
    background: 'white',
    borderRadius: '5px',
    border: '1px solid rgba(0, 0, 0, 0.2)',
  },
  editor: (props) => ({
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    color: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.87)',
    '&.leftAdornment': {
      flexDirection: 'row-reverse',
      width: 'calc(100% - 40px)',
      '& > :first-child': {
        paddingLeft: 0,
      },
    },
    '&.rightAdornment > :first-child': {
      width: 'calc(100% - 40px)',
      paddingRight: 0,
    },
    '& > :first-child': {
      padding: '10px',
      width: '100%',
      boxSizing: 'border-box',
    },
  }),

  adornment: {
    '& > *': {
      width: 35,
      height: 35,
    },
  },
})
