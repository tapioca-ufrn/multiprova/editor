import React, { Component } from 'react'
import classnames from 'classnames'

import Popper from '@material-ui/core/Popper'
import InputBase from '@material-ui/core/InputBase'

import { propTypes } from './propTypes'

export class TexEditorComponent extends Component {
  static propTypes = propTypes
  timeout
  refInterval
  dynamicValuesInterval
  prevDynamicValueLength
  ref

  constructor(props) {
    super(props)
    this.ref = React.createRef()
    this.prevDynamicValueLength = props.dynamicValues.length
    this.state = {
      value: this.toTexEditorValue(props.value),
      open: true,
      dynamicValueError: false,
      dynamicValueErrorName: '',
    }
  }

  componentDidMount = () => {
    this.refInterval = setInterval(this.focusOnRef, 200)
    this.dynamicValuesInterval = setInterval(this.checkDynamicValues)
  }

  componentWillUnmount = () => {
    clearInterval(this.refInterval)
    clearInterval(this.dynamicValuesInterval)
  }

  checkDynamicValues = () => {
    const { dynamicValues } = this.props
    const changedDynamicValuesLength = this.prevDynamicValueLength !== dynamicValues.length
    if (changedDynamicValuesLength) {
      this.onChange({ target: { value: this.state.value } })
    }
    this.prevDynamicValueLength = dynamicValues.length
  }

  focusOnRef = () => {
    const { current } = this.ref
    if (current) {
      clearInterval(this.refInterval)
      const input = current.querySelector('textarea:not([readonly])')
      input.focus()
      const { value } = this.state
      input.setSelectionRange(value.length, value.length)
    }
  }

  reopen = () => {
    const { open } = this.state
    if (open) {
      this.setState({ open: false }, () => {
        this.setState({ open: true })
      })
    }
  }

  onChange = ({ target }) => {
    const value = target.value
    this.setState({ value })
    const dynamicValuesNames = this.props.dynamicValues.map(({ nome }) => nome)
    const dynamicValues = [...value.matchAll(/#(.*?)#/g)].map(([_, dynamicValue]) => dynamicValue)
    const hasEmptyDynamicValue = dynamicValues.includes('')
    const nonexistentDynamicValue =
      dynamicValues.find((dynamicValue) => !dynamicValuesNames.includes(dynamicValue)) || hasEmptyDynamicValue
    if (nonexistentDynamicValue)
      this.setState({
        dynamicValueError: true,
        dynamicValueErrorName: hasEmptyDynamicValue ? '' : nonexistentDynamicValue,
      })
    else this.setState({ dynamicValueError: false, dynamicValueErrorName: '' })
  }

  onBlur = (event) => {
    clearTimeout(this.timeout)
    const { value, dynamicValueError } = this.state
    if (!dynamicValueError) this.props.onExit(this.toEditorValue(value))
    else event.preventDefault()
  }

  sendValue = () => {
    this.props.onChange(this.toEditorValue(this.state.value))
    this.reopen()
  }

  onKeyDown = (event) => {
    clearTimeout(this.timeout)
    const isCloseKeyEvent = event.key === 'Escape'
    if (isCloseKeyEvent) {
      event.preventDefault()
      if (!this.state.dynamicValueError) this.props.onExit(this.toEditorValue(event.target.value))
    } else {
      this.timeout = setTimeout(this.sendValue, 1000)
    }
  }

  toTexEditorValue = (math) => math.replace(/\\fcolorbox{black}{powderblue}{\$(.*?)\$}/gm, (_, group) => `#${group}#`)

  toEditorValue = (math) => math.replace(/#(.*?)#/gm, (_, group) => `\\fcolorbox{black}{powderblue}{$${group}$}`)

  render() {
    const { classes, popperProps } = this.props
    const { value, open, dynamicValueError, dynamicValueErrorName } = this.state
    return (
      <Popper
        {...popperProps}
        open={open}
        keepMounted
        modifiers={{
          flip: { enabled: false },
          preventOverflow: {
            enabled: true,
            boundariesElement: 'window',
          },
        }}
      >
        <div ref={this.ref} className={classnames(classes.root, { error: dynamicValueError })}>
          <InputBase
            value={value}
            onChange={this.onChange}
            onBlur={this.onBlur}
            onKeyDown={this.onKeyDown}
            minRows={3}
            maxRows={15}
            multiline
            fullWidth
            inputProps={{ classes: { root: classes.textField } }}
          />
          {dynamicValueError && <i className={classes.error}>{`O valor #${dynamicValueErrorName}# não existe`}</i>}
        </div>
      </Popper>
    )
  }
}
