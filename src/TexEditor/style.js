import { FOCUS_COLOR } from '../utils'

export const style = () => ({
  root: {
    border: '1px solid ' + FOCUS_COLOR,
    borderRadius: 5,
    background: 'white',
    padding: '5px 10px',
    width: 320,
    '&.error': {
      borderColor: '#f44336',
    },
  },
  textField: {
    fontFamily: 'monospace',
  },
  error: {
    color: '#f44336',
    fontSize: '10pt',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  },
})
