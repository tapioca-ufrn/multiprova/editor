import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  tooltip: PropTypes.string,
  items: PropTypes.array.isRequired,
  active: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  editor: PropTypes.object.isRequired,
}
