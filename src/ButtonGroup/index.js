import { withStyles } from '@material-ui/core/styles'

import { ButtonGroupComponent } from './ButtonGroupComponent'
import { style } from './style'

export const ButtonGroup = withStyles(style)(ButtonGroupComponent)
