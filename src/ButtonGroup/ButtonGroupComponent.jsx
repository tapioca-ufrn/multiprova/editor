import React, { Component, Fragment } from 'react'
import { InlineMath } from 'react-katex'

import Popover from '@material-ui/core/Popover'

import { propTypes } from './propTypes'
import { Button } from '../Button'
import { DYNAMIC_VALUE, insertInline } from '../utils'

export class ButtonGroupComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
    }
  }

  openGroup = (event) => {
    if (this.props.handleToolbarActionSelected) this.props.handleToolbarActionSelected(true)
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
    if (this.props.handleToolbarActionSelected) this.props.handleToolbarActionSelected(false)
  }

  insertDynamicValue = ({ nome, id, tipo }) => {
    const { editor, defaultFontSize } = this.props
    const element = {
      type: DYNAMIC_VALUE,
      data: { nome, id, tipo },
      children: [{ text: '' }],
    }
    insertInline(element, defaultFontSize, editor)
  }

  onClickItem = (item) => () => {
    const { type } = this.props
    if (type === DYNAMIC_VALUE) this.insertDynamicValue(item)
    this.handleClose()
  }

  render() {
    const { items, classes, tooltip, active, children, type } = this.props
    const { anchorEl } = this.state
    return type === DYNAMIC_VALUE ? (
      <Fragment>
        <Button onClick={this.openGroup} tooltip={tooltip} active={active}>
          {children}
        </Button>
        <Popover
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          classes={{ paper: classes.popoverPaper }}
        >
          <div className={classes.popover}>
            {items.map((item, index) => (
              <div key={index} onClick={this.onClickItem(item)}>
                <InlineMath math={item.nome} />
              </div>
            ))}
          </div>
        </Popover>
      </Fragment>
    ) : null
  }
}

ButtonGroupComponent.propTypes = propTypes

ButtonGroupComponent.defaultProps = {}
