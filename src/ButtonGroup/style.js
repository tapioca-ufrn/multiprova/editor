export const style = () => ({
  root: {},
  popover: {
    padding: 5,
    maxWidth: 156,
    maxHeight: 128,
    overflowY: 'scroll',
    '& > *': {
      display: 'inline-block',
      cursor: 'pointer',
      padding: 3,
      width: 29,
      border: 'solid 1px transparent',
      textAlign: 'center',
      '&:hover': {
        borderColor: 'lightblue',
      },
    },
    '&::-webkit-scrollbar': {
      width: 8,
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,0.4)',
      borderRadius: 20,
      border: '3px solid white',
    },
  },
  popoverPaper: {
    overflowY: 'hidden',
  },
  icon: {
    width: '24px',
  },
})
