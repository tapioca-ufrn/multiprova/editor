import isUrl from 'is-url'
import { Transforms } from 'slate'
import { DYNAMIC_VALUE, LINK, MATH, TAB, TEX, VOID_ELEMENTS } from '../utils'

export const withSchema = (editor) => {
  const { isInline, isVoid, insertText } = editor

  editor.isInline = (element) => {
    if ([MATH, TEX].includes(element.type)) return !element.data.block
    else if ([TAB, DYNAMIC_VALUE, LINK].includes(element.type)) return true
    else return isInline(element)
  }

  editor.isVoid = (element) => {
    return VOID_ELEMENTS.includes(element.type) ? true : isVoid(element)
  }

  editor.insertText = (text) => {
    if (text && isUrl(text)) {
      const element = { type: LINK, children: [{ text }], data: { url: text, edit: false } }
      Transforms.insertNodes(editor, element)
      Transforms.insertNodes(editor, { text: '' })
    } else {
      insertText(text)
    }
  }

  return editor
}
