import React from 'react'
import { Path } from 'slate'
import { ReactEditor } from 'slate-react'

import { Node } from '../Node'
import { TableCell, TableRow, Paragraph, Caption, DynamicValue } from '../Nodes'
import {
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  IMAGE,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  TEX,
  TAB,
  PARAGRAPH,
  CONTAINER,
  HORIZONTAL_RULER,
  VIDEO,
  AUDIO,
  CAPTION,
  DYNAMIC_VALUE,
  YOUTUBE_VIDEO,
  LINK,
} from '../utils'

export const renderNode = (elementProps, editorProps) => {
  const { attributes, children, element } = elementProps
  const { editor, isEditorFocused } = editorProps

  let isFocused = false
  if (editor.selection && isEditorFocused) {
    const elementPath = ReactEditor.findPath(editor, element)
    isFocused = Path.compare(elementPath, editor.selection.focus.path) === 0
  }

  const props = { ...elementProps, isFocused }

  switch (element.type) {
    case PARAGRAPH:
      return <Paragraph {...elementProps} {...editorProps} />
    case CAPTION:
      return <Caption {...props} {...editorProps} />
    case BLOCK_QUOTE:
      return <blockquote {...attributes}>{children}</blockquote>
    case BULLETED_LIST:
      return <ul {...attributes}>{children}</ul>
    case LIST_ITEM:
      return <li {...attributes}>{children}</li>
    case NUMBERED_LIST:
      return <ol {...attributes}>{children}</ol>
    case TABLE_ROW:
      return <TableRow {...elementProps} />
    case TABLE_CELL:
      return <TableCell {...props} {...editorProps} />
    case DYNAMIC_VALUE:
      return <DynamicValue {...props} />
    case TEX:
    case MATH:
    case IMAGE:
    case VIDEO:
    case AUDIO:
    case TABLE:
    case CONTAINER:
    case HORIZONTAL_RULER:
    case YOUTUBE_VIDEO:
    case LINK:
      return <Node {...props} {...editorProps} />
    case TAB:
      return (
        <span {...attributes} style={{ paddingLeft: '32px' }}>
          {children}
        </span>
      )
    default:
      return children
  }
}
