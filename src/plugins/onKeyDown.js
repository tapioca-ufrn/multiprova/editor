import { Editor, Path, Range, Transforms } from 'slate'
import { ReactEditor } from 'slate-react'
import {
  isBoldHotkey,
  isItalicHotkey,
  isUnderlinedHotkey,
  isCodeHotkey,
  BOLD,
  ITALIC,
  UNDERLINE,
  CODE,
  isTabHotkey,
  TAB,
  IMAGE,
  TEX,
  MATH,
  isParagraphAboveHotkey,
  insertParagraph,
  insertMath,
  isLatexHotkey,
  insertInline,
  TABLE_CELL,
  PARAGRAPH,
  CONTAINER,
  LIST_ITEM,
  ALIGN_LEFT,
} from '../utils'

const handleHotkey = (event, editor, defaultFontSize) => {
  let mark
  let isAnchorBlockAddParagraph = false
  if (isBoldHotkey(event)) mark = BOLD
  else if (isItalicHotkey(event)) mark = ITALIC
  else if (isUnderlinedHotkey(event)) mark = UNDERLINE
  else if (isCodeHotkey(event)) mark = CODE

  if (editor.selection) {
    const [element] = Editor.above(editor)
    isAnchorBlockAddParagraph = element ? [IMAGE, TEX, MATH].includes(element.type) : false
  }
  const fireParagraphAbove = isParagraphAboveHotkey(event) && isAnchorBlockAddParagraph
  const fireParagraphBelow = event.key === ' ' && isAnchorBlockAddParagraph

  if (mark) {
    event.preventDefault()
    const [node] = Editor.node(editor, editor.selection)
    if (node[mark]) Editor.removeMark(editor, mark)
    else Editor.addMark(editor, mark, true)
  } else if (isTabHotkey(event)) {
    event.preventDefault()
    insertInline({ type: TAB, children: [{ text: '' }] }, defaultFontSize, editor)
    Transforms.move(editor, { unit: 'character', distance: 1 })
  } else if (isLatexHotkey(event)) {
    event.preventDefault()
    insertMath({ value: '', block: false, edit: true, type: TEX }, defaultFontSize, editor)
  } else if (fireParagraphAbove) {
    event.preventDefault()
    insertParagraph(editor, false, defaultFontSize, true)
  } else if (fireParagraphBelow) {
    event.preventDefault()
    insertParagraph(editor, false, defaultFontSize, false)
  }
}

const preventKeyEvent = (event, editor, defaultFontSize) => {
  const [tableCell] = Editor.nodes(editor, { match: (n) => n.type === TABLE_CELL })
  const [container] = Editor.nodes(editor, { match: (n) => n.type === CONTAINER })
  const tableCellOrContainer = tableCell ? tableCell : container ? container : null
  const [elementFocused, pathElementFocused] = Editor.above(editor)
  const keyElementFocused = ReactEditor.findKey(editor, elementFocused)
  const [node] = Editor.node(editor, editor.selection)

  switch (event.key) {
    case 'Backspace': {
      if (tableCellOrContainer) {
        const keyFirstElement = ReactEditor.findKey(editor, tableCellOrContainer[0].children[0])
        const isFirstElement = keyElementFocused.id === keyFirstElement.id
        let isElementBefore = true
        if (isFirstElement && editor.selection.focus.offset === 0) isElementBefore = false
        if (!isElementBefore) event.preventDefault()
      } else if (
        elementFocused.type !== LIST_ITEM &&
        editor.selection.focus.offset === 0 &&
        Range.isCollapsed(editor.selection)
      ) {
        const keyFirstElement = ReactEditor.findKey(editor, editor.children[0])
        const isFirstElement = keyElementFocused.id === keyFirstElement.id
        if (!isFirstElement) {
          const previousPath = Path.previous(pathElementFocused)
          const [previousElement] = Editor.node(editor, previousPath)
          if (![PARAGRAPH, TAB, LIST_ITEM].includes(previousElement.type)) {
            event.preventDefault()
            Transforms.removeNodes(editor, { at: pathElementFocused })
          }
        } else if (![PARAGRAPH, TAB, LIST_ITEM].includes(elementFocused.type)) {
          Transforms.removeNodes(editor, { at: pathElementFocused })
          Transforms.insertNodes(
            editor,
            { type: PARAGRAPH, data: { align: ALIGN_LEFT }, children: [{ text: '' }] },
            { at: pathElementFocused },
          )
          ReactEditor.focus(editor)
          Transforms.select(editor, pathElementFocused)
        }
      }
      if (elementFocused === PARAGRAPH && node.text.length === 0) {
        event.preventDefault()
        if (editor.children.length === 1) insertParagraph(editor, false, defaultFontSize, false)
        Transforms.removeNodes(editor, { at: pathElementFocused })
      }
      break
    }
    case 'Delete': {
      if (tableCellOrContainer) {
        const lastIndex = tableCellOrContainer[0].children.length - 1
        const keyLastElement = ReactEditor.findKey(editor, tableCellOrContainer[0].children[lastIndex])
        const isLastElement = keyElementFocused.id === keyLastElement.id
        let isElementAfter = true
        if (isLastElement && editor.selection.focus.offset === node.text.length) isElementAfter = false
        if (!isElementAfter) event.preventDefault()
      } else if (editor.selection.focus.offset === node.text.length && elementFocused.type !== LIST_ITEM) {
        const lastIndex = editor.children.length - 1
        const keyLastElementInEditor = ReactEditor.findKey(editor, editor.children[lastIndex])
        const isLastElement = keyElementFocused.id === keyLastElementInEditor.id
        if (!isLastElement) {
          const nextPath = Path.next(pathElementFocused)
          const [nextElement] = Editor.node(editor, nextPath)
          if (nextElement.type !== PARAGRAPH) {
            event.preventDefault()
            Transforms.removeNodes(editor, { at: pathElementFocused })
          }
        }
      }
      break
    }
    default:
      break
  }
}

export const onKeyDown = (event, editor, defaultFontSize) => {
  const props = [event, editor, defaultFontSize]
  handleHotkey(...props)
  preventKeyEvent(...props)
}
