import React from 'react'

export const renderMark = (props) => {
  const { children, leaf, attributes } = props
  let mark = children

  if (leaf.bold) {
    mark = <strong>{mark}</strong>
  }
  if (leaf.code) {
    mark = <code>{mark}</code>
  }
  if (leaf.italic) {
    mark = <em>{mark}</em>
  }
  if (leaf.underline) {
    mark = <u>{mark}</u>
  }
  if (leaf.sup) {
    mark = <sup>{mark}</sup>
  }
  if (leaf.sub) {
    mark = <sub>{mark}</sub>
  }
  if (leaf.fontSize) {
    mark = <span style={{ fontSize: leaf.fontSize }}>{mark}</span>
  }

  return (
    <span style={{ paddingLeft: leaf.text === '' ? '0.1px' : null }} {...attributes}>
      {mark}
    </span>
  )
}
