import { withStyles } from '@material-ui/core/styles'

import { ResizableComponent } from './ResizableComponent'
import { style } from './style'

export const Resizable = withStyles(style)(ResizableComponent)
