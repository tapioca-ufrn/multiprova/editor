import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.any.isRequired,
  size: PropTypes.shape({
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
  }),
  minHeight: PropTypes.number,
  minWidth: PropTypes.number,
  depth: PropTypes.number,
  isFocused: PropTypes.bool.isRequired,
  dimensions: PropTypes.bool,
  lockAspectRatio: PropTypes.bool,
  blockHeightResizing: PropTypes.bool,
  adaptive: PropTypes.bool,
  autoResize: PropTypes.bool,
  onResize: PropTypes.func.isRequired,
  onResizeStart: PropTypes.func,
  readOnly: PropTypes.bool,
}
