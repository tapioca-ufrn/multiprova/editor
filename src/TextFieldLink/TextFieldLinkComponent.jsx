import React, { Component } from 'react'
import classnames from 'classnames'

import Popper from '@material-ui/core/Popper'
import InputBase from '@material-ui/core/InputBase'

import { propTypes } from './propTypes'

export class TextFieldLinkComponent extends Component {
  static propTypes = propTypes
  timeout
  refInterval
  ref

  constructor(props) {
    super(props)
    this.ref = React.createRef()
    this.state = {
      value: props.value,
      open: true,
    }
  }

  componentDidMount = () => {
    this.refInterval = setInterval(this.focusOnRef, 200)
    if (this.state.value) this.props.verifyLinkIsValid(this.state.value)
  }

  componentWillUnmount = () => {
    clearInterval(this.refInterval)
  }

  focusOnRef = () => {
    const { current } = this.ref
    if (current) {
      clearInterval(this.refInterval)
      const input = current.querySelector('textarea:not([readonly])')
      input.focus()
      const { value } = this.state
      input.setSelectionRange(value.length, value.length)
    }
  }

  reopen = () => {
    const { open } = this.state
    if (open) {
      this.setState({ open: false }, () => {
        this.setState({ open: true })
      })
    }
  }

  onChange = ({ target }) => {
    const value = target.value
    this.setState({ value })
    this.props.verifyLinkIsValid(value)
  }

  onBlur = (event) => {
    clearTimeout(this.timeout)
    this.props.onBlur(this.state.value)
    event.preventDefault()
  }

  sendValue = () => {
    this.props.sendValue(this.state.value)
    this.reopen()
  }

  onKeyDown = (event) => {
    clearTimeout(this.timeout)
    const isCloseKeyEvent = event.key === 'Escape' || event.key === 'Enter'
    if (isCloseKeyEvent) {
      event.preventDefault()
      this.props.onKeyDown(event.target.value)
    } else {
      this.timeout = setTimeout(this.sendValue, 1000)
    }
  }

  render() {
    const { classes, popperProps, placeholder, errorMessage, isError } = this.props
    const { value, open } = this.state

    return (
      <Popper
        {...popperProps}
        open={open}
        keepMounted
        modifiers={{
          flip: { enabled: false },
          preventOverflow: {
            enabled: true,
            boundariesElement: 'window',
          },
        }}
        className={classes.popper}
      >
        <div ref={this.ref} className={classnames(classes.root, { error: isError })}>
          <InputBase
            value={value}
            onChange={this.onChange}
            onBlur={this.onBlur}
            onKeyDown={this.onKeyDown}
            minRows={3}
            maxRows={15}
            multiline
            fullWidth
            inputProps={{
              classes: { root: classes.textField },
              placeholder,
            }}
          />
          {isError && <i className={classes.error}>{errorMessage}</i>}
        </div>
      </Popper>
    )
  }
}
