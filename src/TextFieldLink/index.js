import { withStyles } from '@material-ui/core/styles'

import { TextFieldLinkComponent } from './TextFieldLinkComponent'
import { style } from './style'

export const TextFieldLink = withStyles(style)(TextFieldLinkComponent)
