import PropTypes from 'prop-types'

export const propTypes = {
  value: PropTypes.string,
  verifyLinkIsValid: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  sendValue: PropTypes.func.isRequired,
  onKeyDown: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  popperProps: PropTypes.object,
  isError: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
  errorMessage: PropTypes.string.isRequired,
}
