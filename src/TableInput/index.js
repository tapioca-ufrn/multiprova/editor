import { withStyles } from '@material-ui/core/styles'

import { TableInputComponent } from './TableInputComponent'
import { style } from './style'

export const TableInput = withStyles(style)(TableInputComponent)
