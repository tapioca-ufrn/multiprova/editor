export const style = () => ({
  root: (props) => ({
    padding: 5,
    position: 'absolute',
    top: -1,
    right: -1,
    background: props.theme === 'highContrast' ? 'black' : 'rgb(240,240,240)',
    zIndex: 3,
    border: props.theme === 'highContrast' ? '1px solid white' : '1px solid rgb(200,200,200)',
    borderRadius: 5,
  }),
  table: {
    borderCollapse: 'collapse',
    width: 150,
    height: 150,
    padding: 5,
  },
  cell: (props) => ({
    border: props.theme === 'highContrast' ? '1px solid white' : '1px solid #eee',
    background: props.theme === 'highContrast' ? 'yellow' : 'white',
    cursor: 'pointer',
    '&.selected': {
      background: props.theme === 'highContrast' ? 'black' : 'rgb(150,150,150)',
    },
  }),
  textField: (props) => ({
    width: 60,
    height: 30,
    '& .MuiInputBase-root': {
      color: props.theme === 'highContrast' ? 'white' : 'rgba(0,0,0,0.87)',
    },
    '& .MuiInput-underline:before': {
      borderColor: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.42)',
    },
    '& .MuiInput-underline:hover': {
      borderColor: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.42)',
    },
    '& .MuiInput-underline:after': {
      borderColor: props.theme === 'highContrast' ? 'white' : '#293a4f',
    },
  }),
  inputs: {
    display: 'flex',
    justifyContent: 'space-around',
    marginBottom: 5,
  },
  buttons: (props) => ({
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 5,
    '& svg': {
      color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)',
      fill: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)',
    },
  }),
})
