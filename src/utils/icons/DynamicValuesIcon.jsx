import React from 'react'

export const DynamicValuesIcon = (props) => {
  return (
    <span style={{ margin: '0 5px', color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)' }}>
      <b>x</b>
    </span>
  )
}
