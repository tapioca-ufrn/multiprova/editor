import { isKeyHotkey } from 'is-hotkey'

export const isBoldHotkey = isKeyHotkey('mod+b')
export const isItalicHotkey = isKeyHotkey('mod+i')
export const isUnderlinedHotkey = isKeyHotkey('mod+u')
export const isCodeHotkey = isKeyHotkey('mod+`')
export const isShiftEnterHotkey = isKeyHotkey('shift+Enter')
export const isParagraphAboveHotkey = isKeyHotkey('shift+ ')
export const isTabHotkey = isKeyHotkey('mod+ ')
export const isLatexHotkey = isKeyHotkey('mod+l')
