import {
  TABLE_ROW,
  TABLE_CELL,
  TABLE,
  PARAGRAPH,
  ALIGN_CENTER,
  VERTICAL_ALIGN_TOP,
  INITIAL_TABLE_CELL_WIDTH,
  INITIAL_TABLE_CELL_HEIGHT,
  SOLID,
  ALIGN_LEFT,
} from '../constants'

const getCells = (row, columns) => {
  const cells = []
  for (let i = 0; i < columns; i++) {
    cells.push(initialCell(row, i))
  }
  return cells
}

const getRows = (rows, columns) => {
  const rowsArray = []
  for (let i = 0; i < rows; i++) {
    rowsArray.push({
      type: TABLE_ROW,
      children: getCells(i, columns),
    })
  }
  return rowsArray
}

export const initialTable = (rows, columns) => ({
  type: TABLE,
  children: getRows(rows, columns),
  data: {
    align: ALIGN_CENTER,
    border: SOLID,
  },
})

export const initialRow = (row, cellsSize) => ({
  type: TABLE_ROW,
  children: getCells(row, cellsSize),
})

export const initialCell = (indexRow, indexColumn) => ({
  indexRow,
  indexColumn,
  type: TABLE_CELL,
  children: [{ type: PARAGRAPH, data: { align: ALIGN_LEFT }, children: [{ text: '', fontSize: '12pt' }] }],
  data: {
    width: INITIAL_TABLE_CELL_WIDTH,
    height: INITIAL_TABLE_CELL_HEIGHT,
    verticalAlign: VERTICAL_ALIGN_TOP,
  },
})
