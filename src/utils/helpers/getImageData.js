export const getImageData = async (file, maxInitialWidth) => {
  const reader = new FileReader()
  reader.readAsDataURL(file)
  const localSrc = await new Promise((resolve) => {
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.onerror = () => {
      throw new Error('Erro ao obter os dados da imagem')
    }
  })
  const dimensions = await new Promise((resolve) => {
    var image = new Image()
    image.onload = () => {
      let width, height
      if (image.width > maxInitialWidth) {
        const factor = image.width / maxInitialWidth
        width = maxInitialWidth
        height = Math.round(image.height / factor)
      } else {
        width = image.width
        height = image.height
      }
      resolve({ width, height })
    }
    image.onerror = () => {
      throw new Error('Erro ao obter os dados da imagem')
    }
    image.src = localSrc
  })
  return { localSrc, ...dimensions }
}
