const replaceToWrittenNumber = (number) =>
  number
    .replace('1', 'one')
    .replace('2', 'two')
    .replace('3', 'three')
    .replace('4', 'four')
    .replace('5', 'five')
    .replace('6', 'six')
    .replace('7', 'seven')
    .replace('8', 'eight')
    .replace('9', 'nine')

export const dynamicValueNameToMQDynamicValue = (name) => `\\var${replaceToWrittenNumber(name)}var`

export const convertMQDynamicValues = (math) =>
  math.replace(/\\fcolorbox{black}{powderblue}{\$(.*?)\$}/gm, (_, group) => `\\var${replaceToWrittenNumber(group)}var `)

export const MQDynamicValuesToLatex = (value) =>
  value.replace(
    /\\var(.*?)var/gm,
    (_, group) =>
      `\\fcolorbox{black}{powderblue}{$${group
        .replace('one', '1')
        .replace('two', '2')
        .replace('three', '3')
        .replace('four', '4')
        .replace('five', '5')
        .replace('six', '6')
        .replace('seven', '7')
        .replace('eight', '8')
        .replace('nine', '9')}$}`,
  )
