import { Path, Transforms } from 'slate'
import { ReactEditor } from 'slate-react'
import { PARAGRAPH, JUSTIFY, ALIGN_LEFT, FONT_SIZE } from '../constants'

export const insertParagraphBelowContainer = (container, editor, editorProps = {}) =>
  insertParagraph(container, true, editor, editorProps)
export const insertParagraphAboveContainer = (container, editor, editorProps = {}) =>
  insertParagraph(container, false, editor, editorProps)

const insertParagraph = (node, isBelow, editor, editorProps) => {
  const { justifyDefault, defaultFontSize } = editorProps
  const parentPath = ReactEditor.findPath(editor, node)
  const nextPath = isBelow ? Path.next(parentPath) : parentPath
  Transforms.insertNodes(
    editor,
    { type: PARAGRAPH, data: { align: justifyDefault ? JUSTIFY : ALIGN_LEFT }, children: [{ text: '' }] },
    { at: nextPath },
  )
  if (defaultFontSize) editor.addMark(FONT_SIZE, defaultFontSize)
}
