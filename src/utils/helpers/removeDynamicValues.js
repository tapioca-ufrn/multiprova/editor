export const removeDynamicValues = (html, dynamicValues) =>
  html.replace(/<span(.*?)>.*?<\/span>/gm, (span, attributes) => {
    const classMatch = attributes.match(/class="(.*?)"/)
    const isMath = Boolean(
      classMatch && classMatch[1] && (classMatch[1].includes('math') || classMatch[1].includes('tex')),
    )
    const isDynamicValue = attributes.includes('data-node-type="dynamic-value"')
    if (isDynamicValue) {
      const name = attributes.match(/data-dynamic-value-nome="(.*?)"/)[1]
      if (dynamicValues.includes(name)) {
        return ''
      }
    } else if (isMath) {
      return span.replace(/\\fcolorbox{black}{powderblue}{\$(.*?)\$}/g, (mathDynamicValue, name) => {
        if (dynamicValues.includes(name)) {
          return ''
        } else return mathDynamicValue
      })
    }
    return span
  })
