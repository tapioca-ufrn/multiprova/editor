export const getVideoData = async (file, maxInitialWidth) => {
  const localSrc = URL.createObjectURL(file)
  const resolution = await new Promise((resolve) => {
    const video = document.createElement('video')
    video.addEventListener('loadedmetadata', () => {
      let width, height
      if (video.videoWidth > maxInitialWidth) {
        const factor = video.videoWidth / maxInitialWidth
        width = maxInitialWidth
        height = Math.round(video.videoHeight / factor)
      } else {
        width = video.videoWidth
        height = video.videoHeight
      }
      resolve({ width, height })
    })
    video.onerror = () => {
      throw new Error('Erro ao obter os dados do vídeo')
    }
    video.src = localSrc
  })
  return { localSrc, ...resolution }
}
