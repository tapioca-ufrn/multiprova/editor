import { Transforms } from 'slate'
import { ReactEditor } from 'slate-react'

export const toggleData = (name, values, node, editor) => {
  const data = node.data
  const index = values.indexOf(data[name])
  const newValue = index === values.length - 1 ? values[0] : values[index + 1]
  const path = ReactEditor.findPath(editor, node)
  Transforms.setNodes(editor, { data: { ...data, [name]: newValue } }, { at: path })
}
