import { ReactEditor } from 'slate-react'
import { Transforms } from 'slate'

export const updateNode = (editor, oldNode, newNode) => {
  const path = ReactEditor.findPath(editor, oldNode)
  Transforms.removeNodes(editor, { at: path })
  Transforms.insertNodes(editor, newNode, { at: path })
}
