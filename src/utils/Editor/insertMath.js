import { insertInline } from './insertInline'
import { PARAGRAPH, MATH, TEX, IMAGE, VIDEO, AUDIO } from '../constants'
import { Editor, Element as SlateElement, Transforms } from 'slate'
import { JUSTIFY } from '../constants'

export const insertMath = ({ value, block = false, edit = false, type = MATH, fontSize }, defaultFontSize, editor) => {
  if (!fontSize) {
    fontSize = defaultFontSize
  }
  const [match] = Array.from(
    Editor.nodes(editor, {
      match: (n) =>
        !Editor.isEditor(n) && SlateElement.isElement(n) && [MATH, TEX, IMAGE, VIDEO, AUDIO].includes(n.type),
    }),
  )
  if (!!match) {
    Transforms.insertNodes(editor, { type: PARAGRAPH, data: { align: JUSTIFY }, children: [{ text: '' }] })
  }
  insertInline(
    {
      type,
      data: {
        value,
        block,
        edit,
        fontSize,
      },
      children: [{ text: '' }],
    },
    defaultFontSize,
    editor,
  )
}
