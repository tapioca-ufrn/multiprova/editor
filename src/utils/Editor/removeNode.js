import { insertParagraphAboveContainer } from '../helpers'
import { insertParagraph } from './insertParagraph'
import { TABLE, CONTAINER, PARAGRAPH, ALIGN_LEFT, TABLE_CELL } from '../constants'
import { ReactEditor } from 'slate-react'
import { Transforms, Editor } from 'slate'

export const removeNode = (node, editor, editorProps = {}) => {
  const { justifyDefault, defaultFontSize } = editor
  const path = ReactEditor.findPath(editor, node)
  const isContainer = [TABLE, CONTAINER].includes(node.type)
  const [parent] = Editor.parent(editor, path)
  Transforms.removeNodes(editor, { at: path })
  if ([TABLE_CELL, CONTAINER].includes(parent.type)) {
    Transforms.insertNodes(
      editor,
      { type: PARAGRAPH, data: { align: ALIGN_LEFT }, children: [{ text: '' }] },
      { at: path },
    )
  }
  if (!editor.isInline(node) && editor.children.length <= 1) {
    if (isContainer) insertParagraphAboveContainer(node, editor, editorProps)
    else insertParagraph(editor, justifyDefault, defaultFontSize, true)
  }
  ReactEditor.focus(editor)
}
