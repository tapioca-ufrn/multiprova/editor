import { Transforms, Editor } from 'slate'
import { DYNAMIC_VALUE, LINK, MATH } from '../constants'

export const insertInline = (element, defaultFontSize, editor) => {
  const { selection } = editor

  if ([DYNAMIC_VALUE, MATH, LINK].includes(element.type)) {
    const path = selection ? selection : [0]
    const [lastNode] = Editor.last(editor, path)
    const activeFontSize = lastNode.fontSize
    const elementFontSize = element.data.fontSize
    const fontSize = elementFontSize ? elementFontSize : activeFontSize ? activeFontSize : defaultFontSize
    element.data.fontSize = fontSize
  }

  Transforms.insertNodes(editor, element)
  Transforms.insertNodes(editor, { text: '' })
}
