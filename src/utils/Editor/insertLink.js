import { insertBlock } from './insertBlock'
import { PARAGRAPH, YOUTUBE_VIDEO, ALIGN_CENTER, JUSTIFY, MATH, TEX, IMAGE, VIDEO, AUDIO } from '../constants'
import { Editor, Element as SlateElement, Transforms } from 'slate'

export const insertLink = ({ type = YOUTUBE_VIDEO, initialWidth = 400 }, editor) => {
  const [match] = Array.from(
    Editor.nodes(editor, {
      match: (n) =>
        !Editor.isEditor(n) &&
        SlateElement.isElement(n) &&
        [MATH, TEX, IMAGE, VIDEO, AUDIO, YOUTUBE_VIDEO].includes(n.type),
    }),
  )
  if (!!match) {
    Transforms.insertNodes(editor, { type: PARAGRAPH, data: { align: JUSTIFY }, children: [{ text: '' }] })
  }

  insertBlock(
    {
      type,
      data: { value: '', edit: true, align: ALIGN_CENTER, width: initialWidth, height: 300 },
      children: [{ text: '' }],
    },
    editor,
  )
}
