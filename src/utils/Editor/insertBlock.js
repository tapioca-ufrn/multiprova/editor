import { JUSTIFY, PARAGRAPH } from '../constants'
import { Editor, Transforms } from 'slate'

export const insertBlock = (block, editor, autoFocus = true) => {
  const { insertNodes, move, setNodes } = Transforms
  const { after, node } = Editor
  const { selection } = editor
  const isLastBlock = after(editor, selection) === undefined
  const [match] = node(editor, selection, { edge: 'end' })
  if (match.text && match.text.length > 0) insertNodes(editor, block)
  else setNodes(editor, block)
  if (isLastBlock) {
    insertNodes(editor, { type: PARAGRAPH, data: { align: JUSTIFY }, children: [{ text: '' }] })
    move(editor, { unit: 'character', distance: 1, reverse: true })
  }
  if (!autoFocus) move(editor, { unit: 'character', distance: 1 })
}
