import { Editor, Path, Transforms } from 'slate'

import { PARAGRAPH, JUSTIFY, ALIGN_LEFT, MARK_TAGS, FONT_SIZE } from '../constants'

export const insertParagraph = (editor, justifyDefault, defaultFontSize, isAbove) => {
  const { selection } = editor
  const defaultAlign = justifyDefault ? JUSTIFY : ALIGN_LEFT
  let align = defaultAlign
  let fontSize = defaultFontSize
  let activeMarkTypes = []
  let path = selection
  if (selection) {
    const lastFragment = Editor.fragment(editor, selection)[0]
    const lastNode = Editor.last(editor, selection)[0]
    activeMarkTypes = Object.keys(lastNode).filter((key) => MARK_TAGS.includes(key))
    if (lastFragment.type === PARAGRAPH) align = lastFragment.data.align
    if (lastNode.fontSize) fontSize = lastNode.fontSize
  }
  const parent = Editor.above(editor)
  path = parent ? parent[1] : [0]
  if (!isAbove) path = Path.next(path)
  Transforms.insertNodes(editor, { type: PARAGRAPH, data: { align }, children: [{ text: '' }] }, { at: path })
  activeMarkTypes.forEach((type) => editor.addMark(type, true))
  editor.addMark(FONT_SIZE, fontSize)
}
