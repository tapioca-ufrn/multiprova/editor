import { Transforms } from 'slate'
import { ReactEditor } from 'slate-react'

import { insertBlock } from './insertBlock'
import { ALIGN_LEFT, JUSTIFY, PARAGRAPH } from '../constants'

export const toggleNodeType = (node, justifyDefault, editor) => {
  const { data } = node
  const { block } = data
  const properties = {
    ...node,
    data: { ...data, block: !block },
  }

  const path = ReactEditor.findPath(editor, node)
  Transforms.removeNodes(editor, { at: path })

  if (block) {
    const newParagraph = {
      type: PARAGRAPH,
      data: { align: justifyDefault ? JUSTIFY : ALIGN_LEFT },
      children: [properties],
    }
    Transforms.insertNodes(editor, newParagraph, { at: path })
    Transforms.move(editor, { unit: 'character', distance: 1 })
  } else insertBlock(properties, editor)
}
