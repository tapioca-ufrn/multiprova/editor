export const basicos = [
  {
    type: 'write',
    value: '+',
    icon: '+',
  },
  {
    type: 'write',
    value: '-',
    icon: '-',
  },
  {
    type: 'write',
    value: '\\sqrt[]{}',
    icon: '\\sqrt[n]{\\phantom{1}}',
    keystrokes: ['Left', 'Left'],
  },
  {
    type: 'cmd',
    value: '\\times',
    icon: '\\times',
  },
  {
    type: 'cmd',
    value: '\\leq',
    icon: '\\leq',
  },
  {
    type: 'cmd',
    value: '\\geq',
    icon: '\\geq',
  },
  {
    type: 'cmd',
    value: '\\div',
    icon: '\\div',
  },
  {
    type: 'write',
    value: '^o',
    icon: 'x^o',
  },
  {
    type: 'write',
    value: '^2',
    icon: 'x^2',
  },
  {
    type: 'write',
    value: '^3',
    icon: 'x^3',
  },
  {
    type: 'typed',
    value: '^',
    icon: 'x^n',
  },
  {
    type: 'typed',
    value: '_',
    icon: 'x_n',
  },
  {
    type: 'typed',
    value: '=',
    icon: '=',
  },
  {
    type: 'typed',
    value: '/',
    icon: '\\frac{x}{n}',
  },
  {
    type: 'cmd',
    value: '\\sqrt',
    icon: '\\sqrt{\\phantom{1}}',
  },

  {
    type: 'cmd',
    value: '\\pm',
    icon: '\\pm',
  },
  {
    type: 'typed',
    value: '<',
    icon: '<',
  },

  {
    type: 'typed',
    value: '>',
    icon: '>',
  },
  {
    type: 'write',
    value: '\\vec{}',
    icon: '\\vec{a}',
    keystrokes: ['Left'],
  },
  {
    type: 'write',
    value: '\\overline{}',
    icon: '\\overline{abc}',
    keystrokes: ['Left'],
  },
  {
    type: 'write',
    value: '\\forall',
    icon: '\\forall',
  },
  {
    type: 'write',
    value: '\\exists',
    icon: '\\exists',
  },
  {
    type: 'write',
    value: '\\cap',
    icon: '\\cap',
  },
  {
    type: 'write',
    value: '\\cup',
    icon: '\\cup',
  },
  {
    type: 'write',
    value: '\\ni',
    icon: '\\ni',
  },
  {
    type: 'write',
    value: '\\in',
    icon: '\\in',
  },
  {
    type: 'write',
    value: '\\propto',
    icon: '\\propto',
  },
  {
    type: 'write',
    value: '\\subset',
    icon: '\\subset',
  },
  {
    type: 'write',
    value: '\\supset',
    icon: '\\supset',
  },
  {
    type: 'write',
    value: '\\subseteq',
    icon: '\\subseteq',
  },
  {
    type: 'write',
    value: '\\supseteq',
    icon: '\\supseteq',
  },
  {
    type: 'write',
    value: '\\emptyset',
    icon: '\\emptyset',
  },
  {
    type: 'write',
    value: '\\nabla',
    icon: '\\nabla',
  },
  {
    type: 'write',
    value: '\\neg',
    icon: '\\neg',
  },
  {
    type: 'write',
    value: '\\angle',
    icon: '\\angle',
  },
  {
    type: 'write',
    value: '\\equiv',
    icon: '\\equiv',
  },
  {
    type: 'write',
    value: '\\neq',
    icon: '\\neq',
  },
  {
    type: 'write',
    value: '\\simeq',
    icon: '\\simeq',
  },
  {
    type: 'write',
    value: '\\approx',
    icon: '\\approx',
  },
  {
    type: 'write',
    value: '\\ll',
    icon: '\\ll',
  },
  {
    type: 'write',
    value: '\\gg',
    icon: '\\gg',
  },
  {
    type: 'write',
    value: '\\perp',
    icon: '\\perp',
  },
  {
    type: 'write',
    value: '\\parallel',
    icon: '\\parallel',
  },
  {
    type: 'write',
    value: '\\infty',
    icon: '\\infty',
  },
  {
    type: 'write',
    value: '\\imath',
    icon: '\\imath',
  },
  {
    type: 'write',
    value: '\\jmath',
    icon: '\\jmath',
  },
  {
    type: 'write',
    value: '\\langle',
    icon: '\\langle',
  },
  {
    type: 'write',
    value: '\\rangle',
    icon: '\\rangle',
  },
]
