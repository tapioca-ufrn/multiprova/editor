import { basicos } from './basicos'
import { calculo } from './calculo'
import { simbolos } from './simbolos'

export const texCommands = [basicos, calculo, simbolos]
