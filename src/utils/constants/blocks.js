import {
  PARAGRAPH,
  BOLD,
  ITALIC,
  UNDERLINE,
  SOB,
  SUB,
  CODE,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  TEX,
  IMAGE,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  TAB,
  CONTAINER,
  HORIZONTAL_RULER,
  VIDEO,
  AUDIO,
  CAPTION,
  DYNAMIC_VALUE,
  FONT_8,
  FONT_10,
  FONT_12,
  FONT_14,
  FONT_18,
  YOUTUBE_VIDEO,
  LINK,
} from './editor'

export const ALIGN_CENTER = 'align-center'
export const ALIGN_LEFT = 'align-left'
export const ALIGN_RIGHT = 'align-right'
export const JUSTIFY = 'justify'
export const BORDER = 'border'
export const DOTTED = 'dotted'
export const VERTICAL_ALIGN_TOP = 'vertical-align-top'
export const VERTICAL_ALIGN_MIDDLE = 'vertical-align-middle'
export const VERTICAL_ALIGN_BOTTOM = 'vertical-align-bottom'
export const SOLID = 'solid'
export const DASHED = 'dashed'
export const DOUBLE = 'double'
export const HIDDEN = 'hidden'
export const LINE_STYLE = 'lineStyle'
export const FONT_SIZE = 'fontSize'

export const EDIT = 'edit'
export const EDIT_LINK = 'edit-link'
export const REMOVE = 'remove'

export const NODE_TAGS = {
  blockquote: BLOCK_QUOTE,
  p: PARAGRAPH,
  ul: BULLETED_LIST,
  ol: NUMBERED_LIST,
  li: LIST_ITEM,
  hr: HORIZONTAL_RULER,
  video: VIDEO,
  audio: AUDIO,
  YOUTUBE_VIDEO: YOUTUBE_VIDEO,
}

export const NODE_CLASSES = {
  image: IMAGE,
  math: MATH,
  table: TABLE,
  'table-row': TABLE_ROW,
  'table-cell': TABLE_CELL,
  tex: TEX,
  tab: TAB,
  container: CONTAINER,
  caption: CAPTION,
  'youtube-video': YOUTUBE_VIDEO,
  link: LINK,
}

export const MARK_TAGS = [ITALIC, BOLD, UNDERLINE, SOB, SUB, CODE]

export const FONT_TYPES = [FONT_8, FONT_10, FONT_12, FONT_14, FONT_18]

export const VOID_ELEMENTS = [HORIZONTAL_RULER, MATH, TEX, TAB, IMAGE, VIDEO, AUDIO, DYNAMIC_VALUE, YOUTUBE_VIDEO]

export const INLINE_ELEMENTS = [MATH, TEX, TAB, DYNAMIC_VALUE, LINK]

export const DATA_TYPES = [ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT, JUSTIFY]

export const LIST_TYPES = [BULLETED_LIST, NUMBERED_LIST]
