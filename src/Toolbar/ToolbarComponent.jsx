/* eslint-disable indent */
import React, { Component } from 'react'
import classnames from 'classnames'

import ExpandMore from '@material-ui/icons/ExpandMore'
import ExpandLess from '@material-ui/icons/ExpandLess'
import Fade from '@material-ui/core/Fade'

import { MathEditor } from '../MathEditor'
import { TableInput } from '../TableInput'
import { Button } from '../Button'
import { actions } from './actions'
import { SelectButton } from '../SelectButton'
import { ButtonGroup } from '../ButtonGroup'
import {
  getImageData,
  PARAGRAPH,
  LIST_ITEM,
  MATH,
  IMAGE,
  ALIGN_CENTER,
  TABLE,
  initialTable,
  TEX,
  TAB,
  BLOCK,
  PARAGRAPH_BELOW,
  PARAGRAPH_ABOVE,
  MARK,
  DATA,
  ALIGN_LEFT,
  ALIGN_RIGHT,
  JUSTIFY,
  updateToolbarPosition,
  CONTAINER,
  HORIZONTAL_RULER,
  SOLID,
  insertBlock,
  VIDEO,
  AUDIO,
  CAPTION,
  getVideoData,
  INITIAL_CONTAINER_WIDTH,
  INITIAL_CONTAINER_HEIGHT,
  MIXED,
  FONT_SIZE,
  insertInline,
  insertMath,
  insertParagraph,
  FONT_TYPES,
  LIST_TYPES,
  YOUTUBE_VIDEO,
  insertLink,
  LINK,
} from '../utils'
import { Editor, Path, Element as SlateElement, Transforms } from 'slate'
import { ReactEditor } from 'slate-react'

export class ToolbarComponent extends Component {
  ref = React.createRef()
  fileInput
  blockInput
  resizeTimeout
  buttonsSize = []
  actions

  constructor(props) {
    super(props)
    const { defaultFontSize, enabledModules } = props
    this.actions = actions({ defaultFontSize, enabledModules })
    this.state = {
      showBlockInput: false,
      showOtherButtons: false,
      numberVisibleButtons: this.actions.length,
      toolbarActionSelected: false,
    }
  }

  componentDidMount() {
    const { floating } = this.props
    this.fileInput = this.ref.current.querySelector('input[type="file"]')
    if (!floating) this.updateButtons()
    window.addEventListener('resize', this.onWindowResize)
  }

  componentWillUnmount() {
    clearTimeout(this.resizeTimeout)
    window.removeEventListener('resize', this.onWindowResize)
  }

  componentDidUpdate(prevProps) {
    const { isEditorFocused, floating } = this.props
    const floatingMakingFocused = floating && isEditorFocused !== prevProps.isEditorFocused && isEditorFocused
    if (floatingMakingFocused) this.updateButtons()
  }

  onWindowResize = () => {
    if (!this.resizeTimeout) {
      this.resizeTimeout = setTimeout(() => {
        const { floating, isEditorFocused } = this.props
        this.resizeTimeout = null
        if (!floating || (floating && isEditorFocused)) this.updateButtons()
      }, 500)
    }
  }

  updateButtons = () => {
    const rootElement = this.ref.current
    if (!rootElement) return
    const getNumberComputedStyle = (element, style) =>
      parseFloat(window.getComputedStyle(element)[style].replace('px', '').replace('auto', '0'))
    const { firstChild: toolbarElement } = rootElement
    if (this.buttonsSize.length === 0)
      this.buttonsSize = [...toolbarElement.firstChild.children].map(
        (button) => button.offsetWidth + getNumberComputedStyle(button, 'marginRight'),
      )
    const toolbarContentWidth = getNumberComputedStyle(toolbarElement, 'width')
    const invisibleToolbar = !toolbarContentWidth
    if (invisibleToolbar) return
    const buttonsSizeSum = (lastIndex) =>
      this.buttonsSize.slice(0, lastIndex).reduce((total, width) => total + width, 0)
    let numberVisibleButtons = this.buttonsSize.length
    while (true) {
      const fits = buttonsSizeSum(numberVisibleButtons) <= toolbarContentWidth
      if (fits) break
      numberVisibleButtons--
    }
    if (numberVisibleButtons === this.buttonsSize.length)
      this.setState({ numberVisibleButtons, showOtherButtons: false })
    else this.setState({ numberVisibleButtons: numberVisibleButtons - 1 })
  }

  isMarkActive = (type) => {
    const { editor, isEditorFocused } = this.props
    if (!isEditorFocused) return false
    const marks = Editor.marks(editor)
    return marks ? marks[type] === true || FONT_TYPES.includes(marks[type]) : false
  }

  onClickMark =
    (type, fontSize = '') =>
    (event = false) => {
      if (event) event.preventDefault()
      const { editor, onClickToolbar } = this.props
      if (!editor.selection) ReactEditor.focus(editor)
      setTimeout(() => {
        const active = this.isMarkActive(type)
        if (this.state.showOtherButtons) this.toggleShowOtherButtons()
        if (active) Editor.removeMark(editor, type)
        else Editor.addMark(editor, type, true)
        if (type === FONT_SIZE) Editor.addMark(editor, type, fontSize)
      }, 100)
      setTimeout(() => {
        onClickToolbar()
      }, 100)
    }

  isBlockActive = (type) => {
    const {
      editor,
      editor: { selection },
      isEditorFocused,
    } = this.props
    if (!selection || !isEditorFocused) return false

    const [match] = Array.from(
      Editor.nodes(editor, {
        at: Editor.unhangRange(editor, selection),
        match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === type,
      }),
    )

    return !!match
  }

  getInicialWidthForYoutubeVideo = () => {
    const [elementFocused] = Editor.nodes(this.props.editor, {
      match: (node) => node.type === TABLE,
    })
    const isInsertingOnTable = elementFocused !== undefined
    return isInsertingOnTable ? 300 : 400
  }

  onClickBlock = (type) => {
    const { editor, classes, dynamicValues, defaultFontSize, justifyDefault, theme } = this.props
    if (this.state.showOtherButtons) this.toggleShowOtherButtons()
    switch (type) {
      case IMAGE:
      case AUDIO:
      case VIDEO: {
        this.fileInput.accept = type === VIDEO ? `${type}/mp4` : type === AUDIO ? `${type}/mp3` : `${type}/*`
        this.fileInput.click()
        return
      }
      case MATH: {
        this.blockInput = (
          <MathEditor
            classes={{ root: classes.mathEditor }}
            theme={theme}
            editor={editor}
            onChange={this.insertMath}
            onCancel={() => {
              this.setState({ showBlockInput: false, toolbarActionSelected: false })
            }}
            dynamicValues={dynamicValues}
          />
        )
        this.setState({ showBlockInput: true, toolbarActionSelected: true })
        return
      }
      case TEX: {
        const [node] = Editor.node(editor, editor.selection)
        insertMath({ fontSize: node.fontSize, value: '', block: false, edit: true, type: TEX }, defaultFontSize, editor)
        return
      }
      case TABLE: {
        this.blockInput = (
          <TableInput
            theme={theme}
            onChange={this.insertTable}
            onCancel={() => {
              this.setState({ showBlockInput: false, toolbarActionSelected: false })
            }}
          />
        )
        this.setState({ showBlockInput: true, toolbarActionSelected: true })
        return
      }
      case TAB: {
        insertInline({ type: TAB, children: [{ text: '' }] }, defaultFontSize, editor)
        Transforms.move(editor, { unit: 'character', distance: 1 })
        return
      }
      case PARAGRAPH_BELOW: {
        insertParagraph(editor, justifyDefault, defaultFontSize, false)
        return
      }
      case PARAGRAPH_ABOVE: {
        insertParagraph(editor, justifyDefault, defaultFontSize, true)
        return
      }
      case CONTAINER: {
        const newParagraph = {
          type: PARAGRAPH,
          data: { align: justifyDefault ? JUSTIFY : ALIGN_LEFT },
          children: [{ text: '' }],
        }
        const container = {
          type,
          data: {
            border: SOLID,
            align: ALIGN_CENTER,
            width: INITIAL_CONTAINER_WIDTH,
            height: INITIAL_CONTAINER_HEIGHT,
          },
          children: [newParagraph],
        }
        this.insertTableOrContainer(container)
        return
      }
      case HORIZONTAL_RULER: {
        insertBlock({ type, data: { lineStyle: SOLID }, children: [{ text: '' }] }, editor, false)
        return
      }
      case CAPTION: {
        // const { anchorBlock } = value
        // if (anchorBlock.type === CAPTION && !anchorBlock.text) {
        //   editor.removeNodeByKey(anchorBlock.key)
        //   editor.insertBlock(PARAGRAPH)
        // }
        // const isMedia = [IMAGE, AUDIO, VIDEO].includes(anchorBlock.type)
        // if (!isMedia) return
        // const align = value.anchorBlock.data.get('align')
        // insertBlock({ type, data: { align } }, editor)
        return
      }
      case YOUTUBE_VIDEO: {
        insertLink({ type: YOUTUBE_VIDEO, initialWidth: this.getInicialWidthForYoutubeVideo() }, editor)
        return
      }
      case LINK: {
        insertInline({ type: LINK, children: [{ text: '' }], data: { url: '', edit: true } }, defaultFontSize, editor)
        return
      }
      default:
        break
    }

    const isActive = this.isBlockActive(type)
    const isList = LIST_TYPES.includes(type)
    Transforms.unwrapNodes(editor, {
      match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && LIST_TYPES.includes(n.type),
      split: true,
    })
    let newProperties
    if (isActive) {
      newProperties = {
        type: PARAGRAPH,
        data: { align: justifyDefault ? JUSTIFY : ALIGN_LEFT },
      }
    } else {
      newProperties = { type: isList ? LIST_ITEM : type }
    }
    Transforms.setNodes(editor, newProperties)
    if (!isActive && isList) {
      const block = { type, children: [] }
      Transforms.wrapNodes(editor, block)
    }
  }

  insertMath = (properties) => {
    const { defaultFontSize, editor } = this.props
    if (properties.value === '') return
    this.setState({ showBlockInput: false })
    const [node] = Editor.node(editor, editor.selection)
    properties.fontSize = node.fontSize
    insertMath(properties, defaultFontSize, editor)
  }

  insertFile = async () => {
    const { withUpload, editor, upload, fileTooLarge } = this.props
    const [elementFocused] = Editor.nodes(editor, {
      match: (node) => node.type === TABLE,
    })
    const isInsertingOnTable = elementFocused !== undefined
    try {
      const file = this.fileInput.files[0]
      let data
      const type = file.type.split('/')[0]
      const maxInitialWidth = isInsertingOnTable ? 300 : 400
      if (file.size <= 100000000) {
        switch (type) {
          case IMAGE:
            const imageData = await getImageData(file, maxInitialWidth)
            data = { ...imageData, loading: withUpload }
            break
          case VIDEO:
            const videoData = await getVideoData(file, maxInitialWidth)
            data = { ...videoData, loading: true }
            break
          case AUDIO:
            data = { localSrc: URL.createObjectURL(file), loading: true }
            break
          default:
            throw new Error('Tipo de arquivo desconhecido')
        }
        const insertedBlock = { type, data: { ...data, align: ALIGN_CENTER }, children: [{ text: '' }] }
        insertBlock(insertedBlock, editor)
        if (withUpload) upload(file, insertedBlock)
      } else fileTooLarge()
    } catch (error) {
      console.error('Erro ao inserir ou enviar o arquivo', error)
    }
    this.fileInput.value = ''
  }

  insertTableOrContainer = (element) => {
    const { editor, justifyDefault } = this.props
    const { selection, children } = editor
    const actualPath = selection ? selection : [children.length - 1]
    const [node, nodePath] = Editor.node(editor, actualPath, { edge: 'end' })
    const [, elementInsertionPath] = Editor.parent(editor, nodePath)
    let nextElementPath = Path.next(elementInsertionPath)

    if (node.text.length) {
      Transforms.insertNodes(editor, element)
      nextElementPath = Path.next(nextElementPath)
    } else {
      Transforms.removeNodes(editor, { at: elementInsertionPath })
      Transforms.insertNodes(editor, element, { at: elementInsertionPath })
    }

    Transforms.insertNodes(
      editor,
      {
        type: PARAGRAPH,
        data: { align: justifyDefault ? JUSTIFY : ALIGN_LEFT },
        children: [{ text: '' }],
      },
      { at: nextElementPath },
    )
    Transforms.move(editor, { unit: 'character', distance: 1, reverse: true })
  }

  insertTable = (rows, columns) => {
    this.setState({ showBlockInput: false }, () => {
      const table = initialTable(rows, columns)
      this.insertTableOrContainer(table)
    })
  }

  onSelect = (formatting) => (type) => {
    switch (formatting) {
      case DATA:
        return this.onClickData(type)()
      case MIXED:
        return this.onClickMark(FONT_SIZE, type)()
      default:
        return () => {}
    }
  }

  onClickData =
    (data) =>
    (event = false) => {
      if (event) event.preventDefault()
      const { editor } = this.props
      if (!editor.selection) ReactEditor.focus(editor)
      setTimeout(() => {
        if (this.state.showOtherButtons) this.toggleShowOtherButtons()
        const isAlign = [ALIGN_CENTER, ALIGN_LEFT, ALIGN_RIGHT, JUSTIFY].includes(data)
        const ALIGNABLE_TYPES = [MATH, AUDIO, VIDEO, IMAGE, TEX, YOUTUBE_VIDEO]
        const blocks = Editor.fragment(editor, editor.selection)
        if (isAlign) {
          const alignableBlocks = blocks.filter(
            ({ type }) =>
              [PARAGRAPH, CAPTION, TABLE, CONTAINER].includes(type) ||
              (ALIGNABLE_TYPES.includes(type) && data !== JUSTIFY),
          )
          alignableBlocks.forEach((block) => {
            Transforms.setNodes(editor, { data: { ...block.data, align: data } })
            if (ALIGNABLE_TYPES.includes(block.type)) updateToolbarPosition()
          })
        }
      }, 250)
      setTimeout(() => {
        this.props.onClickToolbar()
      }, 100)
    }

  renderButton = (
    { formatting, type, Icon, tooltip, defaultIndex = false, select = false, dynamicValuesGroup = false, items },
    index,
  ) => {
    const { classes, editor, dynamicValues, defaultFontSize, theme, onClickToolbar } = this.props
    let isActive,
      onClick = () => () => {}
    switch (formatting) {
      case MARK:
        isActive = this.isMarkActive(type)
        onClick = this.onClickMark
        break
      case BLOCK:
        isActive = this.isBlockActive(type)
        onClick = (type) => (event) => {
          event.preventDefault()
          if (!editor.selection) ReactEditor.focus(editor)
          setTimeout(() => {
            this.onClickBlock(type)
          }, 100)
          setTimeout(() => {
            onClickToolbar()
          }, 100)
        }
        break
      case DATA:
        onClick = this.onClickData
        break
      case MIXED:
        break
      default:
        console.warn('Ação inválida no toolbar do Editor')
        isActive = false
    }
    const defaultProps = { key: index, active: isActive, tooltip, theme }
    let marks = []
    let datasBlock = []
    if (editor.selection) {
      const [paragraph] = Editor.nodes(editor, {
        at: editor.selection,
        match: (n) => [PARAGRAPH, IMAGE, VIDEO, AUDIO, YOUTUBE_VIDEO].includes(n.type),
      })
      const [lastNode] = Editor.last(editor, editor.selection)
      marks = lastNode.fontSize ? [lastNode.fontSize] : []
      if (paragraph) {
        datasBlock = paragraph[0].data && paragraph[0].data.align ? [paragraph[0].data.align] : []
      }
    }
    if (select)
      return (
        <SelectButton
          key={index}
          items={items}
          onSelect={this.onSelect(formatting)}
          datasBlock={datasBlock}
          marks={marks}
          handleToolbarActionSelected={this.handleToolbarActionSelected}
          {...{ defaultIndex, formatting, ...defaultProps }}
        />
      )
    else if (dynamicValuesGroup)
      return (
        <ButtonGroup
          type={type}
          items={dynamicValues}
          editor={editor}
          defaultFontSize={defaultFontSize}
          handleToolbarActionSelected={this.handleToolbarActionSelected}
          {...defaultProps}
        >
          <Icon classes={{ root: classes.icon }} theme={theme} />
        </ButtonGroup>
      )
    else
      return (
        <Button onClick={onClick(type)} {...defaultProps}>
          <Icon classes={{ root: classes.icon }} theme={theme} />
        </Button>
      )
  }

  handleToolbarActionSelected = (value) => {
    this.setState({ toolbarActionSelected: value })
  }

  toggleShowOtherButtons = () => {
    this.setState({ showOtherButtons: !this.state.showOtherButtons })
  }

  render() {
    const { showBlockInput, numberVisibleButtons, showOtherButtons, toolbarActionSelected } = this.state
    const { floating, isEditorFocused, classes, theme } = this.props
    const toolbarButtons = this.actions.slice(0, numberVisibleButtons).map(this.renderButton)
    const otherButtons = this.actions.slice(numberVisibleButtons, this.actions.length).map(this.renderButton)
    return (
      <div ref={this.ref}>
        <div
          className={classnames(classes.root, {
            [classes.rootThemeHighContrast]: theme === 'highContrast',
            [classes.rootThemeLight]: theme !== 'highContrast',
            [classes.floatingToolbar]: false,
            [classes.invisibleToolbar]: floating && !isEditorFocused && !toolbarActionSelected,
          })}
        >
          <div>{toolbarButtons}</div>
          {otherButtons.length > 0 && (
            <div className={theme === 'highContrast' ? classes.moreButtonHighContrast : classes.moreButtonLight}>
              <Button tooltip="Mais" onClick={this.toggleShowOtherButtons} theme={theme}>
                {showOtherButtons ? <ExpandLess /> : <ExpandMore />}
              </Button>
            </div>
          )}
          <Fade in={otherButtons.length > 0 && showOtherButtons} unmountOnExit>
            <div
              className={classnames(classes.otherButtons, {
                [classes.otherButtonsHighContrast]: theme === 'highContrast',
                [classes.otherButtonsLight]: theme !== 'highContrast',
              })}
            >
              {otherButtons}
            </div>
          </Fade>
        </div>
        {showBlockInput && this.blockInput}
        <input type="file" style={{ display: 'none' }} onChange={this.insertFile} />
      </div>
    )
  }
}
