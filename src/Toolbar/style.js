export const style = () => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-start',
    left: '0px !important',
    flexWrap: 'wrap',
    padding: 5,
    position: 'relative',
  },
  rootThemeHighContrast: {
    borderBottom: '2px solid white',
    '& .MuiButton-root.Mui-disabled': {
      color: 'white',
    },
    '& .MuiButtonBase-root.Mui-disabled': {
      color: 'white',
    },
  },
  rootThemeLight: {
    borderBottom: '2px solid rgba(0, 0, 0, 0.15)',
    '& .MuiButton-root.Mui-disabled': {
      color: 'rgba(0, 0, 0, 0.26)',
    },
    '& .MuiButtonBase-root.Mui-disabled': {
      color: 'rgba(0, 0, 0, 0.26)',
    },
  },
  icon: {
    width: '24px',
  },
  mathEditor: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  floatingToolbar: {
    position: 'absolute',
    top: '-27px',
    left: '-1px',
    backgroundColor: 'white',
    border: '1px solid rgba(0,0,0,0.2)',
    borderRadius: '5px 5px 5px 0px',
    padding: '0px 5px',
  },
  invisibleToolbar: { display: 'none' },
  helperText: {
    fontSize: 12,
  },
  otherButtons: {
    position: 'absolute',
    right: '-1px',
    top: '38px',
    padding: 5,
    borderRadius: '0 0 5px 5px',
    zIndex: 2,
  },
  otherButtonsHighContrast: {
    backgroundColor: 'black',
    border: '1px solid white',
  },
  otherButtonsLight: {
    backgroundColor: 'white',
    border: '1px solid rgba(0,0,0,0.2)',
  },
  moreButtonHighContrast: {
    '& svg': {
      color: 'yellow',
      fill: 'yellow',
    },
    '&:hover': {
      '& svg': {
        fill: 'black',
      },
    },
  },
  moreButtonLight: {
    '& svg': {
      color: 'rgba(0, 0, 0, 0.45)',
      fill: 'rgba(0, 0, 0, 0.45)',
    },
    '&:hover': {
      '& svg': {
        fill: 'rgba(0, 0, 0, 0.45)',
      },
    },
  },
})
