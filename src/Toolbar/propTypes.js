import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object,
  focused: PropTypes.bool.isRequired,
  floating: PropTypes.bool.isRequired,
  activeMarks: PropTypes.array.isRequired,
  activeBlocks: PropTypes.array.isRequired,
  onClickMark: PropTypes.func.isRequired,
  onClickBlock: PropTypes.func.isRequired,
  upload: PropTypes.func.isRequired,
  withUpload: PropTypes.bool.isRequired,
  enabledModules: PropTypes.object.isRequired,
  editor: PropTypes.object.isRequired,
  dynamicValues: PropTypes.arrayOf(PropTypes.string),
}
