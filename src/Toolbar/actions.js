import FormatBold from '@material-ui/icons/FormatBold'
import FormatItalic from '@material-ui/icons/FormatItalic'
import FormatUnderlined from '@material-ui/icons/FormatUnderlined'
import Code from '@material-ui/icons/Code'
import FormatListBulleted from '@material-ui/icons/FormatListBulleted'
import FormatListNumbered from '@material-ui/icons/FormatListNumbered'
import FormatQuote from '@material-ui/icons/FormatQuote'
import Functions from '@material-ui/icons/Functions'
import MonetizationOn from '@material-ui/icons/MonetizationOn'
import InsertPhoto from '@material-ui/icons/InsertPhoto'
import BorderAll from '@material-ui/icons/BorderAll'
import KeyboardTab from '@material-ui/icons/KeyboardTab'
import FormatAlignLeft from '@material-ui/icons/FormatAlignLeft'
import FormatAlignRight from '@material-ui/icons/FormatAlignRight'
import FormatAlignJustify from '@material-ui/icons/FormatAlignJustify'
import FormatAlignCenter from '@material-ui/icons/FormatAlignCenter'
import CheckBoxOutlineBlank from '@material-ui/icons/CheckBoxOutlineBlank'
import Remove from '@material-ui/icons/Remove'
import Movie from '@material-ui/icons/Movie'
import MusicVideo from '@material-ui/icons/MusicVideo'
import YouTubeIcon from '@material-ui/icons/YouTube'
import Subtitles from '@material-ui/icons/Subtitles'
import LinkIcon from '@material-ui/icons/Link'

import {
  SuperscriptIcon,
  SubscriptIcon,
  DynamicValuesIcon,
  MARK,
  BLOCK,
  BOLD,
  ITALIC,
  UNDERLINE,
  SOB,
  SUB,
  CODE,
  BULLETED_LIST,
  NUMBERED_LIST,
  BLOCK_QUOTE,
  TAB,
  MATH,
  IMAGE,
  TABLE,
  TEX,
  ParagraphAboveIcon,
  ParagraphBelowIcon,
  PARAGRAPH_BELOW,
  PARAGRAPH_ABOVE,
  DATA,
  ALIGN_LEFT,
  ALIGN_CENTER,
  ALIGN_RIGHT,
  JUSTIFY,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  FONT_10,
  FONT_12,
  FONT_14,
  FONT_18,
  FONT_8,
  MIXED,
  DYNAMIC_VALUE,
  FONT_TYPES,
  YOUTUBE_VIDEO,
  CAPTION,
  LINK,
} from '../utils'

const fontSizeIndex = (fontSize) => FONT_TYPES.findIndex((value) => value.includes(fontSize))

export const actions = ({ defaultFontSize, enabledModules }) =>
  [
    { formatting: MARK, type: BOLD, Icon: FormatBold, tooltip: 'Negrito', enabled: enabledModules.bold },
    { formatting: MARK, type: ITALIC, Icon: FormatItalic, tooltip: 'Itálico', enabled: enabledModules.italic },
    {
      formatting: MARK,
      type: UNDERLINE,
      Icon: FormatUnderlined,
      tooltip: 'Sublinhado',
      enabled: enabledModules.underline,
    },
    { formatting: MARK, type: CODE, Icon: Code, tooltip: 'Código', enabled: enabledModules.code },
    {
      select: true,
      formatting: MIXED,
      items: [
        { type: FONT_8, Icon: '8pt' },
        { type: FONT_10, Icon: '10pt' },
        { type: FONT_12, Icon: '12pt' },
        { type: FONT_14, Icon: '14pt' },
        { type: FONT_18, Icon: '18pt' },
      ],
      defaultIndex: fontSizeIndex(defaultFontSize),
      tooltip: 'Tamanho da fonte',
      enabled: enabledModules.fontSize,
    },
    { formatting: MARK, type: SOB, Icon: SuperscriptIcon, tooltip: 'Sobrescrito', enabled: enabledModules.sob },
    { formatting: MARK, type: SUB, Icon: SubscriptIcon, tooltip: 'Subescrito', enabled: enabledModules.sub },
    {
      select: true,
      formatting: DATA,
      items: [
        { type: ALIGN_LEFT, Icon: FormatAlignLeft, tooltip: 'Alinhar à esquerda' },
        { type: ALIGN_CENTER, Icon: FormatAlignCenter, tooltip: 'Centralizar' },
        { type: ALIGN_RIGHT, Icon: FormatAlignRight, tooltip: 'Alinhar à direita' },
        { type: JUSTIFY, Icon: FormatAlignJustify, tooltip: 'Justificar' },
      ],
      defaultIndex: 0,
      tooltip: 'Alinhamento horizontal',
      enabled: enabledModules.alignment,
    },
    { formatting: BLOCK, type: TAB, Icon: KeyboardTab, tooltip: 'Recuo', enabled: enabledModules.tab },
    { formatting: BLOCK, type: BLOCK_QUOTE, Icon: FormatQuote, tooltip: 'Citação', enabled: enabledModules.quote },
    {
      formatting: BLOCK,
      type: BULLETED_LIST,
      Icon: FormatListBulleted,
      tooltip: 'Lista',
      enabled: enabledModules.bulletedList,
    },
    {
      formatting: BLOCK,
      type: NUMBERED_LIST,
      Icon: FormatListNumbered,
      tooltip: 'Enumeração',
      enabled: enabledModules.numberedList,
    },
    { formatting: BLOCK, type: CAPTION, Icon: Subtitles, tooltip: 'Legenda', enabled: false },
    {
      formatting: BLOCK,
      type: PARAGRAPH_BELOW,
      Icon: ParagraphBelowIcon,
      tooltip: 'Parágrafo abaixo',
      enabled: enabledModules.paragraphBelow,
    },
    {
      formatting: BLOCK,
      type: PARAGRAPH_ABOVE,
      Icon: ParagraphAboveIcon,
      tooltip: 'Parágrafo acima',
      enabled: enabledModules.paragraphAbove,
    },
    { formatting: BLOCK, type: TABLE, Icon: BorderAll, tooltip: 'Tabela', enabled: enabledModules.table },
    {
      formatting: BLOCK,
      type: CONTAINER,
      Icon: CheckBoxOutlineBlank,
      tooltip: 'Grupo',
      enabled: enabledModules.container,
    },
    {
      formatting: BLOCK,
      type: HORIZONTAL_RULER,
      Icon: Remove,
      tooltip: 'Linha',
      enabled: enabledModules.horizontalRuler,
    },
    { formatting: BLOCK, type: MATH, Icon: Functions, tooltip: 'Equações e símbolos', enabled: enabledModules.math },
    { formatting: BLOCK, type: TEX, Icon: MonetizationOn, tooltip: 'Latex', enabled: enabledModules.tex },
    {
      dynamicValuesGroup: true,
      formatting: BLOCK,
      type: DYNAMIC_VALUE,
      Icon: DynamicValuesIcon,
      tooltip: 'Valor',
      enabled: enabledModules.dynamicValue,
    },
    { formatting: BLOCK, type: IMAGE, Icon: InsertPhoto, tooltip: 'Imagem', enabled: enabledModules.image },
    { formatting: BLOCK, type: AUDIO, Icon: MusicVideo, tooltip: 'Áudio', enabled: enabledModules.audio },
    { formatting: BLOCK, type: VIDEO, Icon: Movie, tooltip: 'Vídeo', enabled: enabledModules.video },
    {
      formatting: BLOCK,
      type: YOUTUBE_VIDEO,
      Icon: YouTubeIcon,
      tooltip: 'Vídeo Youtube',
      enabled: enabledModules.youtubeVideo,
    },
    {
      formatting: BLOCK,
      type: LINK,
      Icon: LinkIcon,
      tooltip: 'Link externo',
      enabled: enabledModules.link,
    },
  ].filter(({ enabled }) => enabled)
