import React, { Component } from 'react'
import classnames from 'classnames'
import { Transforms } from 'slate'

import { NodeToolbar } from '../NodeToolbar'
import { Loading } from '../Loading'
import { MathEditor } from '../MathEditor'
import { TexEditor } from '../TexEditor'
import {
  MATH,
  IMAGE,
  TABLE,
  ALIGN_CENTER,
  TEX,
  CONTAINER,
  HORIZONTAL_RULER,
  removeNode,
  VIDEO,
  AUDIO,
  SOLID,
  DOTTED,
  HIDDEN,
  YOUTUBE_VIDEO,
  LINK,
} from '../utils'
import { Resizable } from '../Resizable'
import { Image, Math, Table, Video, Audio, YoutubeVideo, Link } from '../Nodes'
import { propTypes } from './propTypes'
import { HorizontalRuler } from '../Nodes/HorizontalRuler'
import { ReactEditor } from 'slate-react'
import { TextFieldLinkYoutube } from '../TextFieldLinkYoutube'
import { TextFieldExternalLink } from '../TextFieldExternalLink'

export class NodeComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.ref = React.createRef()
    this.state = {
      anchorEl: null,
      openToolbar: true,
      blockEditor: false,
    }
  }

  componentDidMount() {
    const isAlignable = this.hasData('align')
    const nodeElement = this.ref.current
    this.setState({ anchorEl: isAlignable ? nodeElement.firstElementChild : nodeElement }, () => {
      if (this.data.edit) this.edit()
      else this.reopenToolbar()
    })
  }

  get data() {
    return this.props.element.data
  }

  hasData = (property) => this.props.element.data[property]

  reopenToolbar = () => {
    const { openToolbar } = this.state
    if (openToolbar) {
      this.setState({ openToolbar: false })
      setTimeout(() => {
        this.setState({ openToolbar: true })
      }, 200)
    }
  }

  edit = () => {
    const { classes, element, editor, dynamicValues, defaultFontSize } = this.props
    const { anchorEl } = this.state
    const { value, edit } = this.data
    const props = { value, defaultFontSize }
    switch (element.type) {
      case MATH: {
        ReactEditor.blur(editor)
        props.onChange = (properties) => {
          this.setNode(properties)
          this.setState({ blockEditor: false })
          ReactEditor.focus(editor)
        }
        props.onCancel = () => {
          this.setState({ blockEditor: false })
        }
        props.dynamicValues = dynamicValues
        this.blockEditor = <MathEditor {...props} classes={{ root: classes.mathEditor }} />
        break
      }
      case TEX: {
        ReactEditor.blur(editor)
        props.onChange = (value) => {
          this.setNode({ value })
        }
        props.onExit = (value) => {
          if (!value) removeNode(element, editor)
          else {
            this.setNode({ value, edit: !edit })
            this.setState({ blockEditor: false, openToolbar: true })
            ReactEditor.focus(editor)
          }
        }
        props.editor = editor
        props.dynamicValues = dynamicValues
        this.blockEditor = <TexEditor {...props} popperProps={{ anchorEl }} />
        this.setState({ openToolbar: false })
        break
      }
      case YOUTUBE_VIDEO: {
        ReactEditor.blur(editor)
        props.onChange = (value, isValidLink, videoId) => {
          this.setNode({ value, isValidLink, videoId })
        }
        props.onExit = (value, isValidLink, videoId) => {
          if (!value) removeNode(element, editor)
          else {
            this.setNode({ value, edit: !edit, isValidLink, videoId })
            this.setState({ blockEditor: false, openToolbar: true })
            ReactEditor.focus(editor)
          }
        }
        this.blockEditor = <TextFieldLinkYoutube {...props} popperProps={{ anchorEl }} />
        this.setState({ openToolbar: false })
        break
      }
      case LINK: {
        ReactEditor.blur(editor)
        props.onChange = (url) => {
          if (!!element.children[0].text) {
            this.setNode({ url })
          } else {
            const text = url
            this.setNode({ url }, text)
          }
        }
        props.onExit = (url) => {
          if (!url) removeNode(element, editor)
          else {
            if (!!element.children[0].text) {
              this.setNode({ url, edit: !edit })
            } else {
              const text = url
              this.setNode({ url, edit: !edit }, text)
            }
            this.setState({ blockEditor: false, openToolbar: true })
            ReactEditor.focus(editor)
          }
        }
        props.value = this.data.url
        this.blockEditor = <TextFieldExternalLink {...props} popperProps={{ anchorEl }} />
        this.setState({ openToolbar: false })
        break
      }
      default: {
        console.warn('Bloco do Editor não editável')
        this.blockEditor = null
        return
      }
    }
    this.setState({ blockEditor: true })
  }

  setNode = (newData, newText = null) => {
    const { editor, element } = this.props
    const path = ReactEditor.findPath(editor, element)
    const node = { data: { ...this.data, ...newData } }
    Transforms.setNodes(editor, node, { at: path })
    if (newText) {
      const textPath = ReactEditor.findPath(editor, element.children[0])
      Transforms.insertText(editor, newText, { at: textPath })
    }
  }

  get node() {
    const { element, children, editor, readOnly, classes, isFocused } = this.props
    const onFocus = () => {
      const path = ReactEditor.findPath(editor, element)
      Transforms.select(editor, path)
      ReactEditor.focus(editor)
    }
    const resizableProps = ({ width, height, align }) => ({
      size: { width, height },
      isFocused,
      onResize: (size) => {
        this.setNode(size)
        this.reopenToolbar()
      },
      readOnly,
      depth: align === ALIGN_CENTER ? 1 : 2,
      type: element.type,
    })
    switch (element.type) {
      case IMAGE: {
        const { src, localSrc, width, height, align, loading } = this.data
        return (
          <Resizable {...resizableProps({ width, height, align })} blockHeightResizing={false} dimensions>
            <Loading active={loading}>
              <Image src={localSrc ? localSrc : src} />
            </Loading>
          </Resizable>
        )
      }
      case VIDEO: {
        const { src, localSrc, width, height, align, loading } = this.data
        return (
          <Resizable {...resizableProps({ width, height, align })} blockHeightResizing={false} dimensions>
            <Loading active={loading}>
              <Video src={localSrc ? localSrc : src} onFocus={onFocus} />
            </Loading>
          </Resizable>
        )
      }
      case AUDIO: {
        const { src, localSrc, loading } = this.data
        return (
          <Loading active={loading} classes={{ root: classes.audioLoading }}>
            <Audio src={localSrc ? localSrc : src} focus={onFocus} focused={isFocused} />
          </Loading>
        )
      }
      case MATH:
      case TEX: {
        const { value, block, fontSize } = this.data
        return <Math {...{ value, block, fontSize, isFocused }} />
      }
      case TABLE: {
        const { border } = this.data
        return <Table {...{ border, isFocused }}>{children}</Table>
      }
      case CONTAINER: {
        const { border, width, height, align } = this.data
        return (
          <div style={{ display: 'table' }}>
            <div style={{ display: 'table-row' }}>
              <Resizable
                classes={{
                  root: classnames(classes.container, {
                    solid: border === SOLID,
                    dotted: border === DOTTED,
                    hidden: border === HIDDEN,
                  }),
                }}
                {...resizableProps({ width, height, align })}
                lockAspectRatio={false}
                blockHeightResizing={true}
                minHeight={24}
                minWidth={30}
                adaptive={false}
              >
                {children}
              </Resizable>
            </div>
          </div>
        )
      }
      case HORIZONTAL_RULER: {
        const { lineStyle } = this.data
        return <HorizontalRuler {...{ lineStyle, isFocused }} />
      }
      case YOUTUBE_VIDEO: {
        const { width, height, align } = this.data
        return (
          <Resizable
            {...resizableProps({ width: Number(width), height: Number(height), align })}
            blockHeightResizing={false}
            dimensions
          >
            <YoutubeVideo element={element} />
          </Resizable>
        )
      }
      case LINK: {
        const { url } = this.data
        return <Link {...{ isFocused, url, readOnly }}>{children}</Link>
      }
      default: {
        console.warn('Bloco personalizado do Editor desconhecido')
        return null
      }
    }
  }

  get wrapperClasses() {
    const { classes } = this.props
    const { align, block } = this.data
    return classnames(classes.wrapper, {
      [ALIGN_CENTER]: block,
      [align]: !!align,
    })
  }

  get rootClasses() {
    const { classes, element, isFocused } = this.props
    return classnames(classes.root, {
      inline: !this.hasData('block') && ![IMAGE, AUDIO, VIDEO].includes(element.type),
      fullWidth: [HORIZONTAL_RULER, CONTAINER, TABLE, IMAGE, AUDIO, YOUTUBE_VIDEO].includes(element.type),
      focused: isFocused,
      blockMargin: [IMAGE, TABLE, AUDIO, VIDEO, CONTAINER, YOUTUBE_VIDEO].includes(element.type),
      void: [IMAGE, MATH, TEX, HORIZONTAL_RULER, YOUTUBE_VIDEO].includes(element.type),
    })
  }

  render() {
    const { element, editor, attributes, children, justifyDefault, defaultFontSize, isFocused } = this.props
    const { openToolbar, anchorEl, blockEditor } = this.state
    const hasAlign = this.hasData('align')
    const hasBlock = this.hasData('block')
    const contentEditable = [CONTAINER, TABLE, LINK].includes(element.type) ? undefined : false
    return (
      <div {...attributes} className={this.rootClasses} contentEditable={contentEditable}>
        <div ref={this.ref}>
          {hasAlign || hasBlock ? <div className={this.wrapperClasses}>{this.node}</div> : this.node}
          <NodeToolbar
            popperProps={{
              open: isFocused && openToolbar,
              anchorEl,
            }}
            element={element}
            editor={editor}
            reopen={this.reopenToolbar}
            edit={this.edit}
            setNode={this.setNode}
            justifyDefault={justifyDefault}
            defaultFontSize={defaultFontSize}
          />
          {blockEditor && this.blockEditor}
          {![CONTAINER, TABLE, LINK].includes(element.type) && children}
        </div>
      </div>
    )
  }
}
