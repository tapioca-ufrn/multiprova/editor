import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  element: PropTypes.any,
  editor: PropTypes.any,
  isFocused: PropTypes.bool.isRequired,
}
