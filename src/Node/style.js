import { ALIGN_CENTER, ALIGN_RIGHT, ALIGN_LEFT } from '../utils'

export const style = () => ({
  root: {
    '&.fullWidth': {
      width: '100%',
    },
    '&.focused': {
      zIndex: 1,
    },
    '&.inline': {
      display: 'inline-block',
    },
    '&.blockMargin': {
      margin: '5px 0',
    },
    '&.void': {
      userSelect: 'none',
    },
  },
  wrapper: {
    display: 'flex',
    position: 'relative',
    maxWidth: '100%',
    [`&.${ALIGN_CENTER}`]: {
      justifyContent: 'center',
    },
    [`&.${ALIGN_RIGHT}`]: {
      float: 'right',
      marginLeft: '10px',
      zIndex: 1,
    },
    [`&.${ALIGN_LEFT}`]: {
      float: 'left',
      marginRight: '10px',
      zIndex: 1,
    },
  },
  mathEditor: {
    position: 'absolute',
    right: '-10px',
    top: '-12px',
  },
  audioLoading: { marginBottom: '-7px' },
  container: {
    display: 'table-cell',
    padding: '3px',
    border: '1px solid rgba(0,0,0,0.6)',
    '&.dotted': { borderStyle: 'dotted' },
    '&.solid': { borderStyle: 'solid' },
    '&.hidden': { borderStyle: 'hidden' },
  },
})
