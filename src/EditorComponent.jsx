import React, { Component, createRef } from 'react'
import classnames from 'classnames'
import { createEditor, Transforms, Editor, Element as SlateElement } from 'slate'
import { Editable, ReactEditor, Slate, withReact } from 'slate-react'
import { withHistory } from 'slate-history'

import Typography from '@material-ui/core/Typography'
import Collapse from '@material-ui/core/Collapse'

import { Toolbar } from './Toolbar'
import { serializer, deserializer } from './serializer'
import { propTypes } from './propTypes'
import { INITIAL_VALUE, TAB, PARAGRAPH, DYNAMIC_VALUE } from './utils'
import * as plugins from './plugins'
import { defaultProps } from './defaultProps'

import 'katex/dist/katex.min.css'

export class EditorComponent extends Component {
  static propTypes = propTypes
  static defaultProps = defaultProps

  ref = createRef()
  editor = plugins.withSchema(withReact(withHistory(createEditor())))
  lastArrowKey

  constructor(props) {
    super(props)
    const { html, justifyDefault } = props
    this.state = {
      focused: false,
      value: deserializer(html, justifyDefault),
      showAdornment: false,
    }
  }

  componentDidMount() {
    const { readOnly, adornment, onRef } = this.props
    if (readOnly) return
    if (adornment.onEvent) this.addAdornmentListeners()
    if (onRef) onRef(this)
  }

  componentDidUpdate(prevProps) {
    const { placeholder, dynamicValues, notUpdateWhenDynamicValuesChange } = this.props

    if (dynamicValues.length !== prevProps.dynamicValues.length && notUpdateWhenDynamicValuesChange) {
      this.handleChangeDynamicValuesToUpdate()
    }

    if (placeholder !== prevProps.placeholder && this.isEmpty) {
      setTimeout(this.updatePlaceholder, 500)
    }
  }

  get isEmpty() {
    if (!this.editor) return true
    const { value } = this.state
    if (value.length === 1)
      return value[0].text === '' || (value[0].type === PARAGRAPH && value[0].children[0].text === '')
    else if (value.length === 0) return true
    else return false
  }

  updatePlaceholder = () => {
    this.editor.insertText(' ')
    this.editor.deleteBackward({ unit: 'character' })
  }

  handleChangeDynamicValuesToUpdate = () => {
    const { dynamicValues } = this.props

    const newValue = []

    this.state.value.forEach((element) => {
      if (element.type === PARAGRAPH) {
        const newParagraph = { ...element, children: [] }
        element.children.forEach((inlineElement) => {
          if (inlineElement.type === DYNAMIC_VALUE) {
            const existent = dynamicValues.find((dynamicValue) => dynamicValue.id === inlineElement.data.id)
            if (existent) {
              newParagraph.children.push(inlineElement)
            } else {
              const dynamicValueToDelete = ReactEditor.findPath(this.editor, inlineElement)
              Transforms.removeNodes(this.editor, { at: dynamicValueToDelete })
            }
          } else {
            newParagraph.children.push(inlineElement)
          }
        })
        newValue.push(newParagraph)
      } else {
        newValue.push(element)
      }
    })

    this.setState({ value: newValue }, () => {
      this.serializeOnChange()
    })
  }

  addAdornmentListeners = () => {
    const { onEvent } = this.props.adornment
    if (onEvent === 'hover') {
      this.ref.current.addEventListener('mouseenter', () => {
        this.setState({ showAdornment: true })
      })
      this.ref.current.addEventListener('mouseleave', () => {
        this.setState({ showAdornment: false })
      })
    }
  }

  onChange = (value) => {
    this.setState({ value })
    this.handleChange()
  }

  handleChange = () => {
    const { selection } = this.editor

    const [match] = Array.from(
      Editor.nodes(this.editor, {
        at: selection,
        match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === TAB,
      }),
    )

    if (match) {
      switch (this.lastArrowKey) {
        case 'left':
          Transforms.move(this.editor, { unit: 'character', distance: 1, reverse: true })
          break
        case 'right':
          Transforms.move(this.editor, { unit: 'character', distance: 1 })
          break
        default:
          Transforms.move(this.editor, { unit: 'character', distance: 1 })
      }
    }
  }

  onKeyUp = (event) => {
    if (this.props.onKeyUp) this.props.onKeyUp(event, this.serializeOnChange)
  }

  serializeOnChange = () => {
    const serialized = serializer(this.state.value, this.props.dynamicValues)
    this.props.onChange(serialized)
  }

  onClickToolbar = () => {
    if (this.props.onClickToolbar) this.props.onClickToolbar(this.serializeOnChange)
  }

  onKeyDown = ({ key }) => {
    if (key === 'ArrowLeft') this.lastArrowKey = 'left'
    else if (key === 'ArrowRight') this.lastArrowKey = 'right'
  }

  onBlur = () => {
    const { dynamicValues } = this.props
    const serialized = serializer(this.state.value, dynamicValues)
    this.setState({ focused: false })
    this.props.onChange(serialized)
    this.props.onBlur()
  }

  onFocus = () => {
    this.props.onFocus()
    this.setState({ focused: true }, () => {
      ReactEditor.focus(this.editor)
      if (this.isEmpty) Transforms.select(this.editor, [0])
    })
  }

  onPaste = () => {
    setTimeout(() => {
      this.handleChangeDynamicValuesToUpdate()
    }, 1000)
  }

  clear = () => {
    this.onChange({ value: deserializer(INITIAL_VALUE, this.props.justifyDefault) })
  }

  upload = async (file, node) => {
    try {
      const { upload } = this.props
      if (!upload) throw new Error('Método de upload indefinido')
      const src = await upload(file, node.type)
      const data = node.data
      Transforms.setNodes(this.editor, { data: { ...data, src, loading: false } })
    } catch (error) {
      throw error
    }
  }

  get editorClasses() {
    const { classes, adornment } = this.props
    return classnames(classes.editor, {
      rightAdornment: adornment.element && adornment.align === 'right',
      leftAdornment: adornment.element && adornment.align === 'left',
    })
  }

  get toolbarProps() {
    const {
      floatingToolbar: floating,
      upload,
      fileTooLarge,
      dynamicValues,
      defaultFontSize,
      theme,
      enabledModules,
    } = this.props
    const { value, focused } = this.state

    return {
      fileTooLarge,
      editor: this.editor,
      withUpload: !!upload,
      upload: this.upload,
      theme: theme,
      floating,
      isEditorFocused: focused,
      dynamicValues,
      defaultFontSize,
      value,
      enabledModules,
      onClickToolbar: this.onClickToolbar,
    }
  }

  get editorProps() {
    const { placeholder, rows, spellCheck, autoFocus, id, defaultFontSize } = this.props
    const { onBlur, onChange, editor, onFocus, onKeyUp, onPaste } = this
    const { value } = this.state
    const { onKeyDown } = plugins

    return {
      id: id,
      value,
      style: { minHeight: `${rows * 20 + 20}px`, margin: '0 10px' },
      placeholder: placeholder ? placeholder : null,
      onBlur,
      onChange,
      onFocus,
      onKeyDown: (event) => onKeyDown(event, editor, defaultFontSize),
      onKeyUp,
      onPaste,
      spellCheck,
      autoFocus,
      editor,
    }
  }

  get renderEditorProps() {
    const { placeholder, id, justifyDefault, defaultFontSize, dynamicValues, readOnly } = this.props
    const { onChange, editor } = this
    const { value, focused } = this.state

    return {
      id: id,
      value,
      placeholder: placeholder ? placeholder : null,
      onChange,
      justifyDefault,
      defaultFontSize,
      dynamicValues,
      editor,
      readOnly,
      isEditorFocused: focused,
    }
  }

  get readOnlyEditor() {
    const { renderMark, renderNode } = plugins
    return (
      <Slate editor={this.editor} value={this.state.value}>
        <Editable
          renderElement={(props) => renderNode(props, this.renderEditorProps)}
          renderLeaf={renderMark}
          value={this.state.value}
          readOnly
        />
      </Slate>
    )
  }

  render() {
    const { classes, helperText, error, adornment, readOnly, label, className } = this.props
    const { renderMark, renderNode } = plugins
    const { showAdornment, value } = this.state
    const showLabel = Boolean(helperText)
    const DESABLED_PLACEHOLDER_ANIMATION = true
    if (readOnly) return this.readOnlyEditor
    return (
      <div className={classes.root} ref={this.ref}>
        {label && (
          <Typography className={classes.label} gutterBottom>
            {label}
          </Typography>
        )}
        {helperText && (
          <Collapse in={showLabel || DESABLED_PLACEHOLDER_ANIMATION}>
            <Typography variant="caption" gutterBottom color={helperText && error ? 'error' : 'initial'}>
              {helperText ? helperText : null}
            </Typography>
          </Collapse>
        )}
        <Slate editor={this.editor} value={value} onChange={this.onChange}>
          <div className={classnames(classes.container, { [className]: Boolean(className) })}>
            {this.editor && <Toolbar {...this.toolbarProps} />}
            <div className={this.editorClasses} onKeyDown={this.onKeyDown}>
              <Editable
                renderElement={(props) => renderNode(props, this.renderEditorProps)}
                renderLeaf={renderMark}
                {...this.editorProps}
                {...this.props.editorProps}
              />
              {(showAdornment || !adornment.onEvent) && <div className={classes.adornment}>{adornment.element}</div>}
              <div style={{ clear: 'both' }} />
            </div>
          </div>
        </Slate>
      </div>
    )
  }
}
