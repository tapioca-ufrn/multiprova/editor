import React, { Component } from 'react'
import SwipeableViews from 'react-swipeable-views'

import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'

import { propTypes } from './propTypes'

export class TabsComponent extends Component {
  static propTypes = propTypes

  state = {
    tabIndex: 0,
  }

  handleChange = (event, tabIndex) => {
    event.stopPropagation()
    this.setState({ tabIndex })
  }

  handleChangeIndex = (tabIndex) => {
    this.setState({ tabIndex })
  }

  render() {
    const { classes, theme, labels, children } = this.props
    const { tabIndex } = this.state
    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            centered
            value={tabIndex}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
          >
            {labels.map((label, index) => (
              <Tab classes={{ root: classes.scrollButtons }} key={index} label={label} />
            ))}
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={tabIndex}
          onChangeIndex={this.handleChangeIndex}
        >
          {children.map((container, index) => (
            <Typography key={index} component="div" dir={theme.direction} style={{ padding: 10 }}>
              {container}
            </Typography>
          ))}
        </SwipeableViews>
      </div>
    )
  }
}
