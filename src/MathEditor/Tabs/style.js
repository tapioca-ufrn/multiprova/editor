export const style = (theme) => ({
  root: (props) => ({
    '& .MuiAppBar-colorDefault': {
      backgroundColor: props.theme === 'highContrast' ? 'black' : '#f5f5f5',
    },
    '& .MuiTab-textColorPrimary': {
      color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0,0,0,0.54)',
    },
    '& .MuiTab-textColorPrimary.Mui-selected': {
      color: props.theme === 'highContrast' ? 'white' : '#293a4f',
      borderBottomColor: props.theme === 'highContrast' ? 'white' : '#293a4f',
    },
    '& .PrivateTabIndicator-colorPrimary-335': {
      backgroundColor: props.theme === 'highContrast' ? 'white' : '#293a4f',
    },
  }),
  scrollButtons: {
    minWidth: 120,
  },
})
