import React, { Component } from 'react'
import { InlineMath } from 'react-katex'
import Send from '@material-ui/icons/Send'
import Cancel from '@material-ui/icons/Cancel'
import { addStyles, EditableMathField } from '@tapioca/multiprova-mathquill'
import { ReactEditor } from 'slate-react'

import { Tabs } from './Tabs'
import { Button } from '../Button'
import { texCommands, dynamicValueNameToMQDynamicValue, convertMQDynamicValues, MQDynamicValuesToLatex } from '../utils'
import { propTypes } from './propTypes'

addStyles()

export class MathEditorComponent extends Component {
  static propTypes = propTypes
  ref
  mathField
  texCommands = [...texCommands]

  constructor(props) {
    super(props)
    this.ref = React.createRef()
    const valueCommands = this.props.dynamicValues.map(({ nome }) => ({
      type: 'write',
      value: dynamicValueNameToMQDynamicValue(nome),
      icon: nome,
    }))
    this.texCommands.push(valueCommands)
    this.state = {
      value: props.value ? convertMQDynamicValues(props.value) : '',
    }
  }

  createMathField = () => {
    const { value, classes } = this.props
    const input = this.ref.current.querySelector('.' + classes.mathEditorInput)
    const that = this
    const config = {
      spaceBehavesLikeTab: false,
      handlers: {
        enter() {
          that.onChange()
        },
      },
    }
    const MQ = window.MathQuill.getInterface(2)
    this.mathField = MQ.MathField(input, config)
    if (value) {
      this.mathField.latex(convertMQDynamicValues(value))
    }
    this.mathField.focus()
  }

  command =
    ({ type, value, keystrokes }) =>
      (event) => {
        event.stopPropagation()
        switch (type) {
          case 'cmd':
            this.mathField.cmd(value)
            break
          case 'write':
            this.mathField.write(value)
            break
          case 'typed':
            this.mathField.typedText(value)
            break
          default:
            console.warn('Comando MathQuill desconhecido')
        }
        if (keystrokes) {
          keystrokes.forEach((k) => this.mathField.keystroke(k))
        }
        this.mathField.focus()
      }

  onSubmit = (event) => {
    if (event) event.preventDefault()
    const { onChange, dynamicValues, editor } = this.props
    const value = MQDynamicValuesToLatex(this.state.value, dynamicValues)
    onChange({ value })
    if (editor) {
      ReactEditor.focus(editor)
    }
  }

  onMathquillDidMount = (mathField) => {
    mathField.focus()
    this.mathField = mathField
  }

  get commandTabs() {
    return this.texCommands.map((commandTypes) =>
      commandTypes.map((command, index) => (
        <div key={index} className={this.props.classes.command} onClick={this.command(command)}>
          <InlineMath math={command.icon} />
        </div>
      )),
    )
  }

  render() {
    const { classes, onCancel, theme } = this.props
    const that = this
    return (
      <div ref={this.ref} className={classes.root}>
        <Tabs theme={theme} labels={['Básico', 'Cálculo', 'Símbolo', 'Valor']}>
          {this.commandTabs}
        </Tabs>
        <div
          onClick={(event) => {
            event.stopPropagation()
          }}
          className={classes.inputContainer}
        >
          <EditableMathField
            config={{
              restrictMismatchedBrackets: false,
              spaceBehavesLikeTab: false,
              handlers: {
                enter() {
                  that.onSubmit()
                },
              },
            }}
            mathquillDidMount={this.onMathquillDidMount}
            latex={this.state.value}
            onChange={(mathField) => {
              this.setState({ value: mathField.latex() })
            }}
          />
          <div className={classes.containerButton}>
            <Button onClick={this.onSubmit} tooltip="Confirmar">
              <Send />
            </Button>
            <Button onClick={onCancel} tooltip="Cancelar">
              <Cancel />
            </Button>
          </div>
        </div>
      </div>
    )
  }
}
