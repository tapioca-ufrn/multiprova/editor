export const style = () => ({
  root: (props) => ({
    display: 'inline-flex',
    flexDirection: 'column',
    backgroundColor: props.theme === 'highContrast' ? 'black' : 'white',
    zIndex: 3,
    border: props.theme === 'highContrast' ? '1px solid white' : '1px solid lightgray',
    borderRadius: '5px',
    boxShadow: props.theme === 'highContrast' ? 'none' : '1px 1px 10px lightgray',
    maxWidth: 500,

    '& .mq-math-mode': {
      // Solução do bug que faz sumir parte do expoente da raiz
      '& .mq-root-block': {
        padding: '10px 2px',
      },
      // Correção da altura do expoente da raiz
      '& .sup.mq-nthroot': {
        verticalAlign: '0.4em',
      },
    },
  }),

  inputContainer: {
    padding: '0 0px 5px 5px',
    display: 'flex',
    '& > :first-child': {
      width: '100%',
    },
  },

  mathEditorInput: {
    maxWidth: '435px',
    padding: '4px 5px 0',
    flexGrow: 1,
    borderRadius: '5px !important',
    borderColor: 'lightgray !important',
    '&.mq-focused': {
      boxShadow: 'none',
    },
    fontSize: '16pt',
  },

  command: (props) => ({
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    minWidth: '30px',
    cursor: 'default',
    border: '1px solid transparent',
    borderRadius: '2px',
    margin: '3px',
    color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0,0,0,0.87)',
    padding: '3px',
    '&:hover': {
      borderColor: 'lightblue',
    },
  }),
  containerButton: (props) => ({
    display: 'flex',
    '& svg': {
      color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0,0,0,0.45)',
    },
  }),
})
