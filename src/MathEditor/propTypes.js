import PropTypes from 'prop-types'

export const propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  dynamicValues: PropTypes.arrayOf(PropTypes.object),
}
