import React from 'react'
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'

import { propTypes } from './propTypes'

export const ButtonComponent = ({ children, classes, active, onClick, disabled, tooltip, theme }) => {
  const style = active ? { 
    backgroundColor: theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.1)', 
    color: theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
    fill: theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
  } : {}
  return (
    <Tooltip title={!disabled ? tooltip : ''}>
      <Button style={style} classes={{ root: classes.root }} onMouseDown={onClick} disabled={disabled}>
        {children}
      </Button>
    </Tooltip>
  )
}

ButtonComponent.propTypes = propTypes

ButtonComponent.defaultProps = {
  disabled: false,
  active: false,
}
