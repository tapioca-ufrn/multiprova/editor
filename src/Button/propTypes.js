import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
}
