export const style = () => ({
  root: (props) => ({
    minWidth: 0,
    minHeight: 0,
    padding: '2px 5px',
    color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)',
    fill: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)',
    '&:not(:last-child)': {
      marginRight: '2px',
    },
    '&:hover': {
      backgroundColor: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.05)',
      color: props.theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
      fill: props.theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
    },
  }),
})
