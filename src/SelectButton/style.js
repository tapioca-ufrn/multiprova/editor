export const style = () => ({
  root: {},
  item: (props) => ({
    fontSize: '9pt',
    fontWeight: 'bold',
    color: props.theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
    padding: 8,
    '& .MuiListItem-root.Mui-selected': {
      backgroundColor: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.08)',
    },
  }),
  button: (props) => ({
    fontWeight: 'bold',
    color: props.theme === 'highContrast' ? 'yellow' : 'rgba(0, 0, 0, 0.45)',
    '&:hover': {
      backgroundColor: props.theme === 'highContrast' ? 'white' : 'rgba(0, 0, 0, 0.05)',
      color: props.theme === 'highContrast' ? 'black' : 'rgba(0, 0, 0, 0.45)',
    },
  }),
  menu: (props) => ({
    '& ul': {
      backgroundColor: props.theme === 'highContrast' ? 'yellow' : 'white',
    },
  }),
})
