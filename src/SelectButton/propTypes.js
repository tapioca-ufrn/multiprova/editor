import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  formatting: PropTypes.string.isRequired,
  datasBlock: PropTypes.array.isRequired,
  marks: PropTypes.array.isRequired,
  tooltip: PropTypes.string,
  defaultIndex: PropTypes.number,
  items: PropTypes.arrayOf(PropTypes.object),
}
