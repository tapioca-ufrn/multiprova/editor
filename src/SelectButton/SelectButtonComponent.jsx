import React, { Component, Fragment } from 'react'

import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import { Button } from '../Button'
import { propTypes } from './propTypes'
import { MIXED, DATA } from '../utils'

export class SelectButtonComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
    }
  }

  handleClick = (event) => {
    if (this.props.handleToolbarActionSelected) this.props.handleToolbarActionSelected(true)
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
    if (this.props.handleToolbarActionSelected) this.props.handleToolbarActionSelected(false)
  }

  onSelect = (type) => (event) => {
    event.preventDefault()
    this.handleClose()
    return this.props.onSelect(type)
  }

  get activeIndex() {
    const { items, datasBlock, marks, formatting } = this.props
    const checkList = (list) => items.findIndex(({ type }) => list.includes(type))
    const checkMark = checkList(marks)
    const checkBlockData = checkList(datasBlock)
    switch (formatting) {
      case DATA:
        return checkBlockData
      case MIXED:
        return checkBlockData !== -1 ? checkBlockData : checkMark
      default:
        return -1
    }
  }

  renderIcon = (Icon) => (typeof Icon === 'string' ? Icon : <Icon />)

  render() {
    const { items, classes, tooltip, defaultIndex } = this.props
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)
    const { activeIndex } = this
    const isDisabled = activeIndex === -1
    const activeIcon = this.renderIcon(items[!isDisabled ? activeIndex : defaultIndex].Icon)
    return (
      <Fragment>
        <Button onClick={this.handleClick} tooltip={tooltip} classes={{ root: classes.button }}>
          {activeIcon}
        </Button>
        <Menu id="long-menu" anchorEl={anchorEl} open={open} onClose={this.handleClose} className={classes.menu}>
          {items.map(({ type, Icon }, index) => (
            <MenuItem
              key={type}
              className={classes.item}
              selected={activeIndex === index}
              onClick={this.onSelect(type)}
            >
              {this.renderIcon(Icon)}
            </MenuItem>
          ))}
        </Menu>
      </Fragment>
    )
  }
}

SelectButtonComponent.propTypes = propTypes

SelectButtonComponent.defaultProps = {}
