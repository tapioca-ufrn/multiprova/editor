import { withStyles } from '@material-ui/core/styles'

import { SelectButtonComponent } from './SelectButtonComponent'
import { style } from './style'

export const SelectButton = withStyles(style)(SelectButtonComponent)
