import { jsx } from 'slate-hyperscript'
import {
  BLOCK,
  MATH,
  IMAGE,
  TAB,
  NODE_CLASSES,
  TABLE,
  TABLE_ROW,
  TABLE_CELL,
  INITIAL_TABLE_CELL_WIDTH,
  INITIAL_TABLE_CELL_HEIGHT,
  VERTICAL_ALIGN_TOP,
  PARAGRAPH,
  ALIGN_LEFT,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  CAPTION,
  INITIAL_CONTAINER_WIDTH,
  INITIAL_CONTAINER_HEIGHT,
  TEX,
  DYNAMIC_VALUE,
  BLOCK_QUOTE,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BODY_TAG,
  BLOCKQUOTE_TAG,
  P_TAG,
  HR_TAG,
  AUDIO_TAG,
  VIDEO_TAG,
  UL_TAG,
  OL_TAG,
  LI_TAG,
  BR_TAG,
  BREAKLINE,
  YOUTUBE_VIDEO,
  youtubeLink,
  LINK,
} from '../utils'

const pixelToNumber = (value) => Number(value.replace('px', ''))
const replaceMath = (math, delimiter) =>
  math.replace(new RegExp(delimiter, 'g'), '').replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&')

export const deserialize = (el, markAttributes = {}) => {
  const { nodeType, nodeName, textContent, childNodes, classList, style, attributes } = el
  const nodeAttributes = { ...markAttributes }
  const nodeTypeClass = classList && NODE_CLASSES[classList[0]]
  const nodeTypeData = attributes && attributes['data-node-type'] ? attributes['data-node-type'].value : false
  const getData = (data) => attributes[`data-${nodeTypeData}-${data}`].value
  let nodeTypeName

  if (nodeTypeData) nodeTypeName = nodeTypeData
  else if (nodeTypeClass) nodeTypeName = nodeTypeClass
  else nodeTypeName = nodeName

  if (nodeType === Node.TEXT_NODE) {
    return jsx('text', nodeAttributes, textContent)
  } else if (nodeType !== Node.ELEMENT_NODE) {
    return null
  }

  switch (nodeTypeName) {
    case 'STRONG':
      nodeAttributes.bold = true
      break
    case 'EM':
      nodeAttributes.italic = true
      break
    case 'U':
      nodeAttributes.underline = true
      break
    case 'CODE':
      nodeAttributes.code = true
      break
    case 'SUB':
      nodeAttributes.sub = true
      break
    case 'SUP':
      nodeAttributes.sup = true
      break
    case 'SPAN': {
      if (style.fontSize && !classList.length) nodeAttributes.fontSize = style.fontSize
      break
    }
    default:
      break
  }

  const children = Array.from(childNodes)
    .map((node) => deserialize(node, nodeAttributes))
    .flat()

  if (children.length === 0) {
    children.push(jsx('text', nodeAttributes, ''))
  }

  switch (nodeTypeName) {
    case BODY_TAG:
      return jsx('fragment', {}, children)
    case BLOCKQUOTE_TAG:
      return jsx('element', { type: BLOCK_QUOTE }, children)
    case P_TAG: {
      const nodeTagProperties = {
        type: PARAGRAPH,
        data: { align: classList ? classList[0] : ALIGN_LEFT },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case HR_TAG: {
      const nodeTagProperties = {
        type: HORIZONTAL_RULER,
        data: { lineStyle: classList[0] },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case AUDIO_TAG: {
      const nodeTagProperties = {
        type: AUDIO,
        data: {
          src: el.src,
          align: classList[0],
          loading: false,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case VIDEO_TAG: {
      const { src, width, height } = el
      const nodeTagProperties = {
        type: VIDEO,
        data: {
          src,
          width,
          height,
          align: classList[0],
          loading: false,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case DYNAMIC_VALUE: {
      const nodeTagProperties = {
        type: DYNAMIC_VALUE,
        data: {
          nome: getData('nome'),
          tipo: getData('tipo'),
          id: getData('id'),
          fontSize: style.fontSize,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case TEX:
    case MATH: {
      const isBlock = [...classList].includes(BLOCK)
      const delimiter = isBlock ? '/block_tex/' : '/inline_tex/'
      const { fontSize } = style
      const nodeTagProperties = {
        type: nodeTypeName,
        data: {
          value: replaceMath(el.innerHTML, delimiter),
          block: isBlock,
          fontSize,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case IMAGE: {
      const { src, width, height } = el.firstChild
      const nodeTagProperties = {
        type: IMAGE,
        data: {
          src,
          width,
          height,
          align: classList[1],
          loading: false,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case YOUTUBE_VIDEO: {
      const { width, height } = attributes
      const videoId = attributes['data-videoid']
      const nodeTagProperties = {
        type: YOUTUBE_VIDEO,
        data: {
          videoId: videoId.value,
          width: width.value,
          height: height.value,
          align: classList[1],
          value: `${youtubeLink}/${videoId.value}`,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case CAPTION: {
      const align = classList ? classList[1] : ALIGN_LEFT
      const nodeTagProperties = {
        type: CAPTION,
        data: { align },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case TABLE: {
      const nodeTagProperties = {
        type: TABLE,
        data: {
          align: classList[1],
          border: classList[2],
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case TABLE_ROW: {
      const nodeTagProperties = {
        type: TABLE_ROW,
      }
      return jsx('element', nodeTagProperties, children)
    }
    case TABLE_CELL: {
      const { width, height } = style
      const verticalAlign = classList[1]
      const nodeTagProperties = {
        type: TABLE_CELL,
        data: {
          width: width ? pixelToNumber(width) : INITIAL_TABLE_CELL_WIDTH,
          height: height ? pixelToNumber(height) : INITIAL_TABLE_CELL_HEIGHT,
          verticalAlign: verticalAlign ? verticalAlign : VERTICAL_ALIGN_TOP,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case TAB:
      return jsx('element', { type: TAB }, children)
    case CONTAINER: {
      const { width, height } = el.firstChild.style
      const nodeTagProperties = {
        type: CONTAINER,
        data: {
          align: classList[1],
          border: classList[2],
          width: width ? pixelToNumber(width) : INITIAL_CONTAINER_WIDTH,
          height: height ? pixelToNumber(height) : INITIAL_CONTAINER_HEIGHT,
        },
      }
      return jsx('element', nodeTagProperties, children)
    }
    case UL_TAG:
      return jsx('element', { type: BULLETED_LIST }, children)
    case OL_TAG:
      return jsx('element', { type: NUMBERED_LIST }, children)
    case LI_TAG:
      return jsx('element', { type: LIST_ITEM }, children)
    case BR_TAG:
      return jsx('element', { type: BREAKLINE }, children)
    case LINK: {
      const nodeTagProperties = {
        type: LINK,
        data: { url: attributes['data-link'].value },
      }
      return jsx('element', nodeTagProperties, children)
    }
    default:
      return children
  }
}
