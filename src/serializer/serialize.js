import React from 'react'
import classnames from 'classnames'
import { Text } from 'slate'

import {
  PARAGRAPH,
  BULLETED_LIST,
  NUMBERED_LIST,
  LIST_ITEM,
  BLOCK_QUOTE,
  MATH,
  IMAGE,
  TABLE,
  TAB,
  TABLE_ROW,
  TABLE_CELL,
  VERTICAL_ALIGN_TOP,
  ALIGN_LEFT,
  CONTAINER,
  HORIZONTAL_RULER,
  AUDIO,
  VIDEO,
  CAPTION,
  TEX,
  DYNAMIC_VALUE,
  BREAKLINE,
  YOUTUBE_VIDEO,
  LINK,
} from '../utils'

export const serialize = (node, dynamicValues) => {
  if (Text.isText(node)) {
    let string = node.text
    if (node.bold) {
      string = <strong>{string}</strong>
    }
    if (node.code) {
      string = <code>{string}</code>
    }
    if (node.italic) {
      string = <em>{string}</em>
    }
    if (node.underline) {
      string = <u>{string}</u>
    }
    if (node.sup) {
      string = <sup>{string}</sup>
    }
    if (node.sub) {
      string = <sub>{string}</sub>
    }
    if (node.fontSize) {
      string = <span style={{ fontSize: node.fontSize }}>{string}</span>
    }
    return string
  }

  const children = node.children.map((child) => serialize(child, dynamicValues))

  const { type, data } = node
  switch (type) {
    case PARAGRAPH: {
      const { align } = data
      return <p className={align}>{children}</p>
    }
    case CAPTION: {
      const { align } = data
      return (
        <div className={classnames(type, { [align]: align && align !== ALIGN_LEFT })}>
          <small>{children}</small>
        </div>
      )
    }
    case BLOCK_QUOTE:
      return <blockquote>{children}</blockquote>
    case BULLETED_LIST:
      return <ul>{children}</ul>
    case LIST_ITEM:
      return <li>{children}</li>
    case NUMBERED_LIST:
      return <ol>{children}</ol>
    case IMAGE: {
      const { src, width, height, align, loading } = data
      if (loading) return null
      return (
        <div className={classnames(type, align)}>
          <img {...{ src, width, height }} alt="imagem" />
        </div>
      )
    }
    case AUDIO: {
      const { src, align, loading } = data
      if (loading) return null
      return <audio className={classnames(align)} {...{ src }} />
    }
    case VIDEO: {
      const { src, width, height, align, loading } = data
      if (loading) return null
      return <video className={classnames(align)} {...{ src, width, height }} />
    }
    case TEX:
    case MATH: {
      const { value, block, fontSize } = data
      const dynamicValuesMath = []
      ;[...value.matchAll(/\\fcolorbox{black}{powderblue}{\$(.*?)\$}/g)].forEach(([_, dynamicValueNome]) => {
        const isSameNome = ({ nome }) => nome === dynamicValueNome
        if (dynamicValues.some(isSameNome) && !dynamicValuesMath.some(isSameNome))
          dynamicValuesMath.push(dynamicValues.find(isSameNome))
      })
      const delimiter = block ? '/block_tex/' : '/inline_tex/'
      return (
        <span
          className={classnames(type, { block })}
          style={{ fontSize }}
          data-dynamic-values={JSON.stringify(dynamicValuesMath)}
        >
          {delimiter}
          {value}
          {delimiter}
        </span>
      )
    }
    case TABLE: {
      const { border, align } = data
      return (
        <div className={classnames(type, align, border)}>
          <div>{children}</div>
        </div>
      )
    }
    case TABLE_ROW:
      return <div className={type}>{children}</div>
    case TABLE_CELL: {
      const { width, height, verticalAlign } = data
      const style = {}
      style.width = width
      style.height = height
      const isAlignTop = verticalAlign === VERTICAL_ALIGN_TOP
      return (
        <div className={classnames(type, { [verticalAlign]: !isAlignTop })} {...{ style }}>
          {children}
        </div>
      )
    }
    case TAB:
      return <span className={type}></span>
    case CONTAINER: {
      const { border, align, width, height } = data
      const style = { width, height }
      return (
        <div className={classnames(type, align, border)}>
          <div {...{ style }}>{children}</div>
        </div>
      )
    }
    case HORIZONTAL_RULER: {
      const { lineStyle } = data
      return <hr className={lineStyle} />
    }
    case DYNAMIC_VALUE: {
      const { nome, id, tipo, fontSize } = data
      return (
        <span
          data-node-type={type}
          data-dynamic-value-id={id}
          data-dynamic-value-tipo={tipo}
          data-dynamic-value-nome={nome}
          style={{ fontSize }}
        >
          {`/inline_tex/\\fcolorbox{black}{powderblue}{$${nome}$}/inline_tex/`}
        </span>
      )
    }
    case BREAKLINE: {
      return <br />
    }
    case YOUTUBE_VIDEO: {
      const { videoId, width, height, align, loading } = data
      if (loading) return null
      return <div width={width} height={height} data-videoid={videoId} className={classnames(type, align)}></div>
    }
    case LINK: {
      const { url } = data
      return (
        <span className={type} data-link={url}>
          {children}
        </span>
      )
    }
    default:
      return children
  }
}
