import { ALIGN_LEFT, BREAKLINE, CONTAINER, JUSTIFY, PARAGRAPH, TABLE, TABLE_CELL } from '../utils'

export const removeAllBreakLines = (value, justifyDefault) => {
  let normalizedValue = []
  let breakline = -1

  value.forEach((node) => {
    const { type, data, children } = node

    switch (type) {
      case BREAKLINE: {
        breakline++
        const emptyParagraphData = {
          align: type === PARAGRAPH && data ? data.align : justifyDefault ? JUSTIFY : ALIGN_LEFT,
        }
        const emptyParagraph = getNewParagraph(emptyParagraphData, `br-${breakline}`)
        normalizedValue.push(emptyParagraph)
        break
      }

      case PARAGRAPH: {
        const newNodes = removeBreakLine({ breakline, node, justifyDefault, normalizedValue })
        breakline = newNodes.breakline
        normalizedValue = newNodes.normalizedValue

        break
      }

      case TABLE: {
        let tableChildren = []
        let hasBreakline = false
        children.forEach((row, indexRow) => {
          let rowChildren = []
          row.children.forEach((cell, indexColumn) => {
            const newNodes = removeBreakLine({
              breakline,
              node: { ...cell, indexColumn, indexRow },
              justifyDefault,
              normalizedValue: rowChildren,
            })
            breakline = newNodes.breakline
            rowChildren = newNodes.normalizedValue
            hasBreakline = newNodes.hasBreakline
          })
          tableChildren.push({ ...row, children: rowChildren })
        })

        if (hasBreakline) normalizedValue.push({ ...node, children: tableChildren })
        else normalizedValue.push(node)

        break
      }

      case CONTAINER: {
        const newNodes = removeBreakLine({ breakline, node, justifyDefault, normalizedValue })
        breakline = newNodes.breakline
        normalizedValue = newNodes.normalizedValue

        break
      }

      default:
        normalizedValue.push(node)
    }
  })

  return normalizedValue
}

const getNewParagraph = (data, key, children = []) => {
  const newParagraph = { type: PARAGRAPH, data, children: children.length ? children : [{ text: '' }] }
  if (key.length) newParagraph.key = key
  return newParagraph
}

const removeBreakLine = ({ breakline, node, justifyDefault, normalizedValue }) => {
  const { type, data, children } = node
  const emptyParagraphData = {
    align: type === PARAGRAPH && data ? data.align : justifyDefault ? JUSTIFY : ALIGN_LEFT,
  }
  let hasBreakline = false
  let lastChildIndex = 0
  let elementChildren = []

  for (let i = 0; i < children.length; i++) {
    const child = children[i]

    if (child.type === BREAKLINE) {
      const nodesBeforeBreakline = children.slice(lastChildIndex, i)
      breakline++
      hasBreakline = true
      lastChildIndex = i + 1

      if (nodesBeforeBreakline.length > 0) {
        if ([CONTAINER, TABLE_CELL].includes(type)) elementChildren = [...elementChildren, ...nodesBeforeBreakline]
        else normalizedValue.push(getNewParagraph(data, '', nodesBeforeBreakline))
      }

      const emptyParagraph = getNewParagraph(emptyParagraphData, `br-${breakline}`)

      if ([CONTAINER, TABLE_CELL].includes(type)) elementChildren.push(emptyParagraph)
      else normalizedValue.push(emptyParagraph)
    }
  }

  if (hasBreakline && lastChildIndex < children.length) {
    const nodesAfterBreakline = children.slice(lastChildIndex)

    if ([CONTAINER, TABLE_CELL].includes(type)) elementChildren = [...elementChildren, ...nodesAfterBreakline]
    else normalizedValue.push(getNewParagraph(data, '', nodesAfterBreakline))
  }

  if (elementChildren.length) normalizedValue.push({ ...node, children: elementChildren })

  if (!hasBreakline && type !== TABLE_CELL) normalizedValue.push(node)

  return { breakline, normalizedValue, hasBreakline }
}
