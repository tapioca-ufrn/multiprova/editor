import { renderToStaticMarkup } from 'react-dom/server'
import { serialize } from './serialize'
import { deserialize } from './deserialize'
import { removeAllBreakLines } from './removeAllBreakLines'
import { ALIGN_LEFT, INITIAL_VALUE, JUSTIFY } from '../utils'

export const serializer = (nodes, dynamicValues) => {
  let stringHtml = ''

  nodes.forEach((node) => {
    const element = serialize(node, dynamicValues)
    const html = renderToStaticMarkup(element)
    stringHtml = stringHtml.concat(html)
  })

  return stringHtml
}

export const deserializer = (htmlString, justifyDefault) => {
  if (!htmlString || !htmlString.length) htmlString = INITIAL_VALUE

  const domParser = new DOMParser()
  let html = domParser.parseFromString(htmlString, 'text/html')

  const alignment = justifyDefault ? JUSTIFY : ALIGN_LEFT

  const isText = html.body.childNodes.length === 1 && html.body.childNodes[0].nodeType === Node.TEXT_NODE
  if (isText) {
    const textWithParagraph = `<p class="${alignment}">${htmlString}</p>`
    html = domParser.parseFromString(textWithParagraph, 'text/html')
  }

  const value = deserialize(html.body)
  const normalizedValue = removeAllBreakLines(value, justifyDefault)

  return normalizedValue
}
