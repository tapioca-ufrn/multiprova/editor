const borderColor = 'rgba(0,0,0,0.2)'

export const style = () => ({
  toolbar: {
    backgroundColor: 'white',
    border: `1px solid ${borderColor}`,
    borderRadius: 5,
    marginTop: 5,
    zIndex: 1,
  },
  icon: {
    width: 24,
  },
  actionGroups: {
    '& > *': {
      display: 'inline-block',
      '&:not(:only-child):first-child': {
        borderRight: `1px solid ${borderColor}`,
        paddingRight: 3,
      },
      '&:not(:only-child):last-child': {
        paddingLeft: 3,
      },
    },
  },
})
