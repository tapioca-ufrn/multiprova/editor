import React, { Component } from 'react'
import classnames from 'classnames'

import Popper from '@material-ui/core/Popper'
import Fade from '@material-ui/core/Fade'
import Paper from '@material-ui/core/Paper'

import { Button } from '../Button'
import { propTypes } from './propTypes'
import { tableCellAction } from '../Nodes/TableCell'
import {
  displayAction,
  removeAction,
  editAction,
  tableCellActions,
  borderAction,
  containerActions,
  HorizontalRulerAction,
  editLinkAction,
} from './actions'
import {
  REMOVE,
  EDIT,
  BLOCK,
  BORDER,
  TABLE,
  REMOVE_COLUMN,
  REMOVE_ROW,
  TABLE_CELL,
  PARAGRAPH_ABOVE,
  PARAGRAPH_BELOW,
  CONTAINER,
  ALIGN_CENTER,
  ALIGN_RIGHT,
  ALIGN_LEFT,
  insertParagraphBelowContainer,
  insertParagraphAboveContainer,
  HORIZONTAL_RULER,
  LINE_STYLE,
  removeNode,
  toggleNodeType,
  DOTTED,
  SOLID,
  HIDDEN,
  DASHED,
  DOUBLE,
  toggleData,
  TABLE_ROW,
  LINK,
  YOUTUBE_VIDEO,
  EDIT_LINK,
} from '../utils'
import { Editor } from 'slate'

const INVALID_ACTION = 'Ação inválida no toolbar flutuante do Editor'

export class NodeToolbarComponent extends Component {
  static propTypes = propTypes
  isTable
  isContainer

  constructor(props) {
    super(props)
    const { element } = props
    this.isTable = element.type === TABLE
    this.isContainer = [TABLE, CONTAINER].includes(element.type)
  }

  get actives() {
    const { element, editor } = this.props
    const { align, block } = element.data
    const actives = []
    if (align) actives.push(align)
    if (block) actives.push(BLOCK)
    if (this.isTable) {
      const [cell] = Editor.nodes(editor, {
        match: (node) => node.type === TABLE_CELL,
      })
      if (cell) {
        const [node] = cell
        const cellVerticalAlign = node.data.verticalAlign
        actives.push(cellVerticalAlign)
      }
    }
    return actives
  }

  get disabled() {
    const { element, editor } = this.props
    const disabled = []
    if (this.isTable) {
      const [row] = Editor.nodes(editor, {
        match: (node) => node.type === TABLE_ROW,
      })
      if (row) {
        const [node] = row
        const isLoneColumn = node.children.length === 1
        if (isLoneColumn) disabled.push(REMOVE_COLUMN)
      }
      const isLoneRow = element.children.length === 1
      if (isLoneRow) disabled.push(REMOVE_ROW)
    }
    return disabled
  }

  get actions() {
    const { element } = this.props
    const { data } = element
    const isHorizontalRuler = element.type === HORIZONTAL_RULER
    const isLink = element.type === LINK || element.type === YOUTUBE_VIDEO
    const withEdit = data && data.value && !isLink
    const withDisplay = data && 'block' in data
    const primary = [removeAction]
    const secondary = []
    if (this.isTable) secondary.push(...tableCellActions)
    if (withDisplay) primary.unshift(displayAction)
    if (this.isContainer) primary.unshift(borderAction, ...containerActions)
    if (withEdit) primary.unshift(editAction)
    if (isHorizontalRuler) primary.unshift(HorizontalRulerAction)
    if (isLink) primary.unshift(editLinkAction)
    return [primary, secondary]
  }

  containerAction = (action) => {
    const { editor, element, reopen, setNode, justifyDefault, defaultFontSize } = this.props
    const editorProps = { justifyDefault, defaultFontSize }
    switch (action) {
      case PARAGRAPH_BELOW:
        insertParagraphBelowContainer(element, editor, editorProps)
        break
      case PARAGRAPH_ABOVE: {
        insertParagraphAboveContainer(element, editor, editorProps)
        break
      }
      case ALIGN_CENTER:
      case ALIGN_RIGHT:
      case ALIGN_LEFT:
        setNode({ align: action })
        reopen()
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  command = (action) => {
    const { element, editor, edit, justifyDefault, defaultFontSize } = this.props
    switch (action) {
      case REMOVE:
        removeNode(element, editor, { justifyDefault, defaultFontSize })
        break
      case EDIT:
      case EDIT_LINK:
        edit()
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  toggle = (action) => {
    const { element, editor, justifyDefault } = this.props
    switch (action) {
      case BLOCK:
        toggleNodeType(element, justifyDefault, editor)
        break
      case BORDER: {
        if ([TABLE, CONTAINER].includes(element.type)) toggleData(BORDER, [SOLID, DOTTED, HIDDEN], element, editor)
        break
      }
      case LINE_STYLE:
        toggleData(LINE_STYLE, [SOLID, DASHED, DOUBLE, DOTTED], element, editor)
        break
      default:
        console.warn(INVALID_ACTION)
    }
  }

  onChange = (action) => {
    const { element, editor } = this.props
    const { command, toggle, tableCell, container } = action
    if (!!command) this.command(command)
    else if (!!toggle) this.toggle(toggle)
    else if (this.isTable && !!tableCell) tableCellAction(element, editor, tableCell)
    else if (this.isContainer && !!container) this.containerAction(container)
  }

  render() {
    const { classes, popperProps } = this.props
    return (
      <Popper {...popperProps} transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <div className={classnames(classes.toolbar, 'toolbar')}>
              <Paper>
                <div className={classes.actionGroups}>
                  {this.actions.map((type) =>
                    !type.length ? null : (
                      <div key={type}>
                        {type.map(({ action, Icon, tooltip }) => {
                          const actionValue = Object.values(action)[0]
                          const isActive = this.actives.includes(actionValue)
                          const isDisabled = this.disabled.includes(actionValue)
                          return isDisabled ? null : (
                            <Button
                              key={actionValue}
                              active={isActive}
                              tooltip={tooltip}
                              onClick={(event) => {
                                event.preventDefault()
                                if (!!action.toggle || !isActive) this.onChange(action)
                              }}
                            >
                              <Icon classes={{ root: classes.icon }} />
                            </Button>
                          )
                        })}
                      </div>
                    ),
                  )}
                </div>
              </Paper>
            </div>
          </Fade>
        )}
      </Popper>
    )
  }
}
