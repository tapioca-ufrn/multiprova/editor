import { withStyles } from '@material-ui/core/styles'

import { EditorComponent } from './EditorComponent'
import { style } from './style'

export const Editor = withStyles(style)(EditorComponent)
