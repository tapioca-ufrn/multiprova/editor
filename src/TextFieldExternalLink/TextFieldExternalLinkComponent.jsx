import React, { Component } from 'react'

import { propTypes } from './propTypes'
import { TextFieldLink } from '../TextFieldLink'

export class TextFieldExternalLinkComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      url: props.value,
    }
  }

  verifyLinkIsValid = (value) => {
    let url = value
    if (!value.startsWith('https') && !value.startsWith('http')) {
      url = `http://${value}`
    }
    this.setState({ url })
  }

  onBlur = () => {
    const { url } = this.state
    this.props.onExit(url)
  }

  sendValue = () => {
    const { url } = this.state
    this.props.onChange(url)
  }

  onKeyDown = () => {
    const { url } = this.state
    this.props.onExit(url)
  }

  render() {
    const { popperProps } = this.props
    const { url } = this.state

    return (
      <TextFieldLink
        placeholder="https://www.url.com"
        errorMessage="Formato válido de link: https://www.url.com"
        isError={false}
        verifyLinkIsValid={this.verifyLinkIsValid}
        onBlur={this.onBlur}
        sendValue={this.sendValue}
        onKeyDown={this.onKeyDown}
        popperProps={popperProps}
        value={url}
      />
    )
  }
}
