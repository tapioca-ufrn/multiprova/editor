import React, { Component } from 'react'

import { propTypes } from './propTypes'
import { TextFieldLink } from '../TextFieldLink'

export class TextFieldLinkYoutubeComponent extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      isValidLink: false,
      errorValidLink: false,
      videoId: '',
    }
  }

  verifyLinkIsValid = (value) => {
    const youtubeRegex =
      /^(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:(?:youtube\.com|youtu.be))(?:\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(?:\S+)?$/
    const matches = value.match(youtubeRegex)
    if (matches !== null && matches.length === 2) {
      this.setState({ isValidLink: true, errorValidLink: false, videoId: matches[1] })
    } else {
      this.setState({
        isValidLink: false,
        errorValidLink: true,
        videoId: '',
      })
    }
  }

  onBlur = (value) => {
    const { isValidLink, videoId } = this.state
    this.props.onExit(value, isValidLink, videoId)
  }

  sendValue = (value) => {
    const { isValidLink, videoId } = this.state
    this.props.onChange(value, isValidLink, videoId)
  }

  onKeyDown = (value) => {
    const { isValidLink, videoId } = this.state
    this.props.onExit(value, isValidLink, videoId)
  }

  render() {
    const { popperProps, value } = this.props
    const { errorValidLink, isValidLink } = this.state
    const isError = !isValidLink && errorValidLink

    return (
      <TextFieldLink
        placeholder="https://www.youtube.com/watch?v=idDoVideo"
        errorMessage="Formato válido de link: https://www.youtube.com/watch?v=idDoVideo"
        isError={isError}
        verifyLinkIsValid={this.verifyLinkIsValid}
        onBlur={this.onBlur}
        sendValue={this.sendValue}
        onKeyDown={this.onKeyDown}
        popperProps={popperProps}
        value={value}
      />
    )
  }
}
