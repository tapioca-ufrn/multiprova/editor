import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  editor: PropTypes.object.isRequired,
  node: PropTypes.object.isRequired,
  isFocused: PropTypes.bool.isRequired,
}
