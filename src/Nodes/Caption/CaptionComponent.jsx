import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'
import { ALIGN_LEFT } from '../../utils'

export class CaptionComponent extends Component {
  static propTypes = propTypes

  get classes() {
    const { classes, node } = this.props
    const { align } = node.data.toJS()
    return classnames(classes.root, { [ALIGN_LEFT]: !align, [align]: !!align })
  }

  render() {
    const { children, attributes } = this.props
    return (
      <div {...attributes} className={this.classes}>
        <small>{children}</small>
      </div>
    )
  }
}
