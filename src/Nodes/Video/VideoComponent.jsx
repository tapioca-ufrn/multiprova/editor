import React, { Component } from 'react'

import { propTypes } from './propTypes'

export class VideoComponent extends Component {
  static propTypes = propTypes
  ref = React.createRef()

  componentDidMount() {
    ;['play', 'pause', 'seeking', 'volumechange'].forEach((eventName) => {
      this.ref.current.addEventListener(eventName, () => {
        this.props.onFocus()
      })
    })
  }

  render() {
    const { classes, src } = this.props
    return (
      <video ref={this.ref} className={classes.root} controls>
        <source src={src} />
      </video>
    )
  }
}
