import { withStyles } from '@material-ui/core/styles'

import { YoutubeVideoComponent } from './YoutubeVideoComponent'
import { style } from './style'

export const YoutubeVideo = withStyles(style)(YoutubeVideoComponent)
