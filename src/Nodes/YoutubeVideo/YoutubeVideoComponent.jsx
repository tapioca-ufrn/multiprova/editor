import React, { Component } from 'react'
import classnames from 'classnames'
import { propTypes } from './propTypes'
import { youtubeLink, multiprovaLink } from '../../utils'

export class YoutubeVideoComponent extends Component {
  static propTypes = propTypes

  get data() {
    return this.props.element.data
  }

  get classes() {
    const { classes } = this.props
    const { align } = this.data
    return classnames(classes.root, align)
  }

  render() {
    const { element } = this.props
    const { data } = element
    const { width, height, videoId } = data

    return (
      <iframe
        className={this.classes}
        width={width}
        height={height}
        src={`${youtubeLink}/${videoId}?origin=${multiprovaLink}`}
        title="YouTube video player"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        allowFullScreen
      />
    )
  }
}
