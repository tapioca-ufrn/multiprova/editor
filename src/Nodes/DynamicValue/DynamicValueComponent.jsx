import React, { Component } from 'react'
import { InlineMath } from 'react-katex'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class DynamicValueComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, element, isFocused, attributes, children } = this.props
    const { fontSize, nome } = element.data
    return (
      <div
        className={classnames(classes.root, { focused: isFocused })}
        style={{ fontSize }}
        contentEditable={false}
        {...attributes}
      >
        <InlineMath>{`\\fcolorbox{${isFocused ? 'purple' : 'black'}}{powderblue}{$${nome}$}`}</InlineMath>
        {children}
      </div>
    )
  }
}
