import PropTypes from 'prop-types'

export const propTypes = {
  element: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
}
