import { withStyles } from '@material-ui/core/styles'

import { DynamicValueComponent } from './DynamicValueComponent'
import { style } from './style'

export const DynamicValue = withStyles(style)(DynamicValueComponent)
