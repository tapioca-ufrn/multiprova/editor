import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class AudioComponent extends Component {
  static propTypes = propTypes
  ref = React.createRef()

  componentDidMount() {
    ;['play', 'pause', 'seeking', 'volumechange'].forEach((eventName) => {
      this.ref.current.addEventListener(eventName, () => {
        this.props.focus()
      })
    })
  }

  render() {
    const { classes, src, focused } = this.props
    return (
      <audio ref={this.ref} className={classnames(classes.root, { focused })} controls>
        <source src={src} />
      </audio>
    )
  }
}
