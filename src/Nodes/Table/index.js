import { withStyles } from '@material-ui/core/styles'

import { TableComponent } from './TableComponent'
import { style } from './style'

export const Table = withStyles(style)(TableComponent)
