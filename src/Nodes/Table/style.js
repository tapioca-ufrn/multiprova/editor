import { HIDDEN, DOTTED } from '../../utils'

const TABLE_CELL = ' > * > *'

export const style = () => ({
  root: {
    display: 'table',
    borderCollapse: 'collapse',
    ['&' + TABLE_CELL]: {
      border: '1px solid rgba(0,0,0,0.6)',
    },
    ['&.opaqueBorder' + TABLE_CELL]: {
      borderColor: 'rgba(0,0,0,0.1)',
      borderStyle: 'solid !important',
    },
    [`&.${HIDDEN}${TABLE_CELL}`]: {
      borderStyle: 'hidden',
    },
    [`&.${DOTTED}${TABLE_CELL}`]: {
      borderStyle: 'dotted',
    },
  },
})
