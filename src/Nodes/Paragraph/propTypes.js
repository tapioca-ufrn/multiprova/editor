import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  editor: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,
}
