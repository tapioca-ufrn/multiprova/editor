import React, { Component } from 'react'
import classnames from 'classnames'
import { Editor, Transforms } from 'slate'

import { ALIGN_RIGHT, ALIGN_CENTER, JUSTIFY, FONT_SIZE, ALIGN_LEFT } from '../../utils'
import { propTypes } from './propTypes'

export class ParagraphComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()

  componentDidMount() {
    setTimeout(this.init, 100)
  }

  init = () => {
    const { editor, readOnly, justifyDefault } = this.props
    if (readOnly) return
    const hasAlignment = !!this.data.align
    if (!hasAlignment) {
      Transforms.setNodes(editor, { data: { ...this.data, align: justifyDefault ? JUSTIFY : ALIGN_LEFT } })
    }
    this.applyDefaultFontSize()
  }

  componentDidUpdate() {
    if (this.props.readOnly) return
    this.handlePlaceholderAlignment()
  }

  handlePlaceholderAlignment = () => {
    const { editor, element, placeholder } = this.props
    const { align } = this.data
    const isEmptyEditor = editor.children.length === 1 && !element.text
    const spans = this.ref.current.getElementsByTagName('span')
    const placeholderElement = [...spans].find((span) => span.innerText === placeholder)
    if (placeholderElement && isEmptyEditor) {
      if ([ALIGN_RIGHT, ALIGN_CENTER].includes(align) && placeholderElement.style.display !== 'none')
        placeholderElement.style.display = 'none'
      else if (![ALIGN_RIGHT, ALIGN_CENTER].includes(align) && placeholderElement.style.display !== 'inline-block')
        placeholderElement.style.display = 'inline-block'
    }
  }

  applyDefaultFontSize = () => {
    const { editor, defaultFontSize } = this.props
    if (editor.selection) {
      const [lastNode] = Editor.last(editor, editor.selection)
      const hasFontSize = !!lastNode.fontSize
      if (!hasFontSize) Editor.addMark(editor, FONT_SIZE, defaultFontSize)
    }
  }

  get data() {
    return this.props.element.data
  }

  get classes() {
    const { classes } = this.props
    const { align } = this.data
    return classnames(classes.root, align)
  }

  render() {
    const { children, attributes } = this.props
    return (
      <div {...attributes} className={this.classes}>
        <div ref={this.ref}>{children}</div>
      </div>
    )
  }
}
