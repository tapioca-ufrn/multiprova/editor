import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  value: PropTypes.string.isRequired,
  fontSize: PropTypes.string.isRequired,
  block: PropTypes.any.isRequired,
  isFocused: PropTypes.bool.isRequired,
}
