import React, { Component } from 'react'
import classnames from 'classnames'
import { InlineMath, BlockMath } from 'react-katex'

import { propTypes } from './propTypes'

export class MathComponent extends Component {
  static propTypes = propTypes

  ref = React.createRef()

  componentDidUpdate() {
    const selector = this.props.block ? '.katex-display' : '.base'
    const outlineElement = this.ref.current.querySelector(selector)
    if (outlineElement) outlineElement.id = 'focusOutline'
  }

  renderError = () => <span style={{ color: 'red' }}>[A sintaxe Latex é inválida]</span>

  render() {
    const { value, block, isFocused, fontSize, classes } = this.props
    const Component = block ? BlockMath : InlineMath
    return (
      <div ref={this.ref} style={{ fontSize }} className={classnames(classes.root, { focused: isFocused })}>
        {value && <Component math={value} renderError={this.renderError} />}
      </div>
    )
  }
}
