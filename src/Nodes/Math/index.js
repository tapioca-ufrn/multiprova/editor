import { withStyles } from '@material-ui/core/styles'

import { MathComponent } from './MathComponent'
import { style } from './style'

export const Math = withStyles(style)(MathComponent)
