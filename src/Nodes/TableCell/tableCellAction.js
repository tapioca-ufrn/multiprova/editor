import { Editor, Transforms } from 'slate'

import {
  initialRow,
  initialCell,
  ADD_ROW_BELOW,
  ADD_ROW_ABOVE,
  ADD_COLUMN_LEFT,
  ADD_COLUMN_RIGHT,
  REMOVE_ROW,
  REMOVE_COLUMN,
  VERTICAL_ALIGN_BOTTOM,
  VERTICAL_ALIGN_MIDDLE,
  VERTICAL_ALIGN_TOP,
  updateToolbarPosition,
  updateNode,
  TABLE_CELL,
  TABLE_ROW,
} from '../../utils'
import { ReactEditor } from 'slate-react'

export const tableCellAction = (table, editor, action) => {
  const [cellSelected] = Editor.nodes(editor, {
    match: (node) => node.type === TABLE_CELL,
  })
  const [cell, pathCell] = cellSelected
  const newSelection = {
    anchor: {
      path: editor.selection.anchor.path,
      offset: 0,
    },
    focus: {
      path: editor.selection.focus.path,
      offset: 0,
    },
  }
  const cellsSize = table.children[0].children.length
  const setCellNode = (properties) => {
    Transforms.setNodes(editor, { data: { ...cell.data, ...properties } }, { at: pathCell })
  }
  switch (action) {
    case ADD_ROW_BELOW: {
      addRow(table, editor, cell.indexRow + 1, cellsSize)
      Transforms.setSelection(editor, newSelection)
      updateToolbarPosition()
      break
    }
    case ADD_ROW_ABOVE: {
      addRow(table, editor, cell.indexRow, cellsSize)
      Transforms.setSelection(editor, newSelection)
      updateToolbarPosition()
      break
    }
    case ADD_COLUMN_LEFT: {
      addColumn(table, editor, cell.indexColumn)
      Transforms.setSelection(editor, newSelection)
      break
    }
    case ADD_COLUMN_RIGHT: {
      addColumn(table, editor, cell.indexColumn + 1)
      Transforms.setSelection(editor, newSelection)
      break
    }
    case REMOVE_ROW: {
      const [row] = Editor.nodes(editor, {
        match: (node) => node.type === TABLE_ROW,
      })
      const [, rowPath] = row
      Transforms.removeNodes(editor, { at: rowPath })
      break
    }
    case REMOVE_COLUMN: {
      table.children.forEach((row) => {
        row.children.forEach((cellRow) => {
          if (cellRow.indexColumn === cell.indexColumn) {
            const columnToDelete = ReactEditor.findPath(editor, cellRow)
            Transforms.removeNodes(editor, { at: columnToDelete })
          }
        })
      })
      break
    }
    case VERTICAL_ALIGN_BOTTOM:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_BOTTOM })
      break
    case VERTICAL_ALIGN_MIDDLE:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_MIDDLE })
      break
    case VERTICAL_ALIGN_TOP:
      setCellNode({ verticalAlign: VERTICAL_ALIGN_TOP })
      break
    default:
      console.warn('Tipo de ação desconhecido na célula da tabela')
  }
}

const addRow = (table, editor, indexRow, cellsSize) => {
  const newRow = initialRow(indexRow, cellsSize)
  const rows = [...table.children]
  rows.splice(indexRow, 0, newRow)
  const rowsUpdated = rows.map((row, index) => {
    const children = row.children.map((cell) => ({ ...cell, indexRow: index }))
    return { ...row, children }
  })
  const newTable = { ...table, children: rowsUpdated }
  updateNode(editor, table, newTable)
}

const addColumn = (table, editor, indexColumn) => {
  const rowsUpdated = table.children.map((row, indexRow) => {
    let children = [...row.children]
    children.splice(indexColumn, 0, initialCell(indexRow, indexColumn))
    children = children.map((cell, indexCell) => ({ ...cell, indexColumn: indexCell }))
    return { ...row, children }
  })
  const newTable = { ...table, children: rowsUpdated }
  updateNode(editor, table, newTable)
}
