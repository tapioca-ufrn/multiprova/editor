import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object.isRequired,
  editor: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,
  isFocused: PropTypes.bool.isRequired,
  children: PropTypes.any.isRequired,
  attributes: PropTypes.any.isRequired,
}
