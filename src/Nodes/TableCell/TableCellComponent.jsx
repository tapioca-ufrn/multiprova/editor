import React, { Component } from 'react'
import classnames from 'classnames'
import { Transforms } from 'slate'
import { ReactEditor } from 'slate-react'

import { propTypes } from './propTypes'
import { Resizable } from '../../Resizable'
import { INITIAL_TABLE_CELL_HEIGHT, INITIAL_TABLE_CELL_WIDTH, updateToolbarPosition } from '../../utils'

export class TableCellComponent extends Component {
  static propTypes = propTypes

  setSize = (size) => {
    const { editor, element } = this.props
    const elementPath = ReactEditor.findPath(editor, element)
    Transforms.setNodes(editor, { data: { ...element.data, ...size } }, { at: elementPath })
    updateToolbarPosition()
  }

  get resizableProps() {
    const { isFocused, element } = this.props
    const { width, height } = element.data
    return {
      size: { width, height },
      isFocused,
      adaptive: false,
      autoResize: isFocused,
      lockAspectRatio: false,
      blockHeightResizing: true,
      onResize: this.setSize,
      minHeight: INITIAL_TABLE_CELL_HEIGHT,
      minWidth: INITIAL_TABLE_CELL_WIDTH,
      type: element.type,
    }
  }

  get verticalAlign() {
    return this.props.element.data['verticalAlign'].replace('vertical-align-', '')
  }

  render() {
    const { children, classes, attributes } = this.props
    const { verticalAlign } = this
    return (
      <Resizable
        {...this.resizableProps}
        {...attributes}
        classes={{
          root: classnames(classes.root, {
            [classes.verticalAlignMiddle]: verticalAlign === 'middle',
            [classes.verticalAlignBottom]: verticalAlign === 'bottom',
          }),
        }}
      >
        <div role="cell">
          <div className={classes.invisible}>&#8205;</div>
          {children}
        </div>
      </Resizable>
    )
  }
}
