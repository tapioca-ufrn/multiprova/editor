export const style = () => ({
  root: {
    display: 'table-cell',
    padding: 3,
  },
  invisible: {
    height: 0,
    visibility: 'hidden',
  },
  verticalAlignMiddle: { verticalAlign: 'middle' },
  verticalAlignBottom: { verticalAlign: 'bottom' },
})
