import PropTypes from 'prop-types'

export const propTypes = {
  classes: PropTypes.object,
  attributes: PropTypes.object,
  url: PropTypes.string,
  isFocused: PropTypes.bool,
  readOnly: PropTypes.bool,
}
