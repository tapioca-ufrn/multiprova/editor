import React, { Component } from 'react'
import classnames from 'classnames'

import { propTypes } from './propTypes'

export class LinkComponent extends Component {
  static propTypes = propTypes

  render() {
    const { classes, attributes, children, url, isFocused, readOnly } = this.props

    return (
      <div className={classnames(classes.root, { focused: isFocused })} {...attributes}>
        {readOnly && (
          <a target="_blank" rel="noreferrer" href={url}>
            {children}
          </a>
        )}
        {!readOnly && <span className={classes.link}>{children}</span>}
      </div>
    )
  }
}
