import { FOCUS_COLOR } from '../../utils'

export const style = () => ({
  root: {
    display: 'inline-block',
    '&.focused': {
      outline: `2px solid ${FOCUS_COLOR}`,
    },
  },
  link: {
    color: 'blue',
    textDecoration: 'underline',
    margin: 0,
  },
})
