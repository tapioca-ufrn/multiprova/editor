import { withStyles } from '@material-ui/core/styles'

import { LinkComponent } from './LinkComponent'
import { style } from './style'

export const Link = withStyles(style)(LinkComponent)
