# Multiprova - Editor

Editor HTML do Multiprova.

## Como instalar?

Para instalar o `@multiprova/editor` em seu projeto, instale a biblioteca como dependência via npm:

```shell
$ npm install @multiprova/editor
```

## Como usar?

A utilização do `@multiprova/editor` é simplificada, basta importar em um componente e começar a usar:

```js
import React, { Component } from 'react'
import { Editor } from '@multiprova/editor'

export default class Example extends Component {
  render() {
    return <Editor />
  }
}
```

O componente se apresentará da seguinte maneira:

<p align="center">
  <img alt="Componente Editor" src=".gitlab/multiprova-editor-example.jpg" />
</p>

### Propriedades (Props)

O `<Editor />` recebe algumas _props_ que modificam diretamente seu comportamento:

| Nome da propriedade | Descrição                                                          |            Tipo             |             Valor padrão             |
| :-----------------: | ------------------------------------------------------------------ | :-------------------------: | :----------------------------------: |
|       `html`        | Elemento html usado para encapsular o texto                        |          `string`           |              `<p></p>`               |
|       `rows`        | Número de linhas                                                   |          `number`           |                 `1`                  |
|  `floatingToolbar`  | Barra de ferramentas flutuante                                     |          `boolean`          |               `false`                |
|  `justifyDefault`   | Alinhamento textual justificado                                    |          `boolean`          |                `true`                |
|    `spellCheck`     | Verificação ortográfica                                            |          `boolean`          |                `true`                |
|     `readOnly`      | Somente leitura                                                    |          `boolean`          |               `false`                |
|     `adornment`     | Adorno do campo <br> Recebe um objeto [Adornment](#####-adornment) |          `object`           | `{ align: 'right', onEvent: false }` |
|    `placeholder`    | Texto de descrição do campo                                        |          `string`           |                 `''`                 |
|     `autoFocus`     | Foco automático                                                    |          `boolean`          |               `false`                |
|     `onChange`      | Função disparada ao alterar o valor do texto                       |         `function`          |              `() => {}`              |
|      `onFocus`      | Função disparada quando o campo está em foco                       |         `function`          |              `() => {}`              |
|      `onBlur`       | Função disparada quando o campo perde o foco                       |         `function`          |              `() => {}`              |
|  `enabledModules`   | Objeto com módulos ativo e inativas para a barra de ferramentas    |          `object`           |                 `{}`                 |
|      `upload`       | Função de upload de mídia                                          | `function (file, nodeType)` |               `false`                |
|  `defaultFontSize`  | Tamanho padrão do texto                                            |          `string`           |               `"12pt"`               |
|   `dynamicValues`   | Conjunto de valores possíveis de uma questão                       |         `object[]`          |                 `[]`                 |

#### Objetos

##### Adornment

```js
{
  align: 'left' | 'right',
  onEvent: false | 'hover' | 'focus' | 'blur',
  element: Elemento React
}
```

## Como executar localmente

Para fazer modificações na biblioteca `@multiprova/editor` localmente siga os seguintes passos:

1. Instale a ferramenta Yalc globalmente:

```shell
$ npm install -g yalc
```

2. Edite o arquivo `package.json` do editor e adicione o seguinte atributo ao objeto:

```json
"files": [
  "src"
]
```

3. Acesse o repositório do editor no terminal e execute:

```shell
$ cd editor
$ yalc push
```

4. Acesse no terminal o repositório que utiliza o editor como dependência e execute:

```shell
$ cd nome-repositorio
$ yalc link @multiprova/editor
```

5. Faça a importação do editor

```js
import { Editor } from '@multiprova/editor/src'
```

6. Para compilar qualquer alteração feita no editor, execute `yalc push` no terminal do `@multiprova/editor` e o repositório que o utiliza como dependência irá compilar automaticamente. **Não** é preciso executar `npm start` no editor.

## Como remover o repositório local

Para remover o repositório local das dependências, acesse o repositório que utiliza o editor como dependência e execute:

```shell
$ cd nome-repositorio
$ yalc remove @multiprova/editor
$ npm ci
```

**ATENÇÃO**: Desfaça as alterações feitas no arquivo `package.json` do editor e remova o '/src' do caminho da importação do editor. É importante que essas alterações não estejam em um commit para o repositório.
